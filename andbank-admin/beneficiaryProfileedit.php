<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg11.php" ?>
<?php include_once "ewmysql11.php" ?>
<?php include_once "phpfn11.php" ?>
<?php include_once "beneficiaryProfileinfo.php" ?>
<?php include_once "userfn11.php" ?>
<?php

//
// Page class
//

$beneficiaryProfile_edit = NULL; // Initialize page object first

class cbeneficiaryProfile_edit extends cbeneficiaryProfile {

	// Page ID
	var $PageID = 'edit';

	// Project ID
	var $ProjectID = "{270D70C4-A473-4EEA-B457-A25A3D8EB6E2}";

	// Table name
	var $TableName = 'beneficiaryProfile';

	// Page object name
	var $PageObjName = 'beneficiaryProfile_edit';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME]);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		$GLOBALS["Page"] = &$this;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (beneficiaryProfile)
		if (!isset($GLOBALS["beneficiaryProfile"]) || get_class($GLOBALS["beneficiaryProfile"]) == "cbeneficiaryProfile") {
			$GLOBALS["beneficiaryProfile"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["beneficiaryProfile"];
		}

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'edit', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'beneficiaryProfile', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect();
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if (!$Security->IsLoggedIn()) {
			$Security->SaveLastUrl();
			$this->Page_Terminate(ew_GetUrl("login.php"));
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $conn, $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $beneficiaryProfile;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($beneficiaryProfile);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		$conn->Close();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $DbMasterFilter;
	var $DbDetailFilter;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;

		// Load key from QueryString
		if (@$_GET["id"] <> "") {
			$this->id->setQueryStringValue($_GET["id"]);
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Process form if post back
		if (@$_POST["a_edit"] <> "") {
			$this->CurrentAction = $_POST["a_edit"]; // Get action code
			$this->LoadFormValues(); // Get form values
		} else {
			$this->CurrentAction = "I"; // Default action is display
		}

		// Check if valid key
		if ($this->id->CurrentValue == "")
			$this->Page_Terminate("beneficiaryProfilelist.php"); // Invalid key, return to list

		// Validate form if post back
		if (@$_POST["a_edit"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = ""; // Form error, reset action
				$this->setFailureMessage($gsFormError);
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues();
			}
		}
		switch ($this->CurrentAction) {
			case "I": // Get a record to display
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("beneficiaryProfilelist.php"); // No matching record, return to list
				}
				break;
			Case "U": // Update
				$this->SendEmail = TRUE; // Send email on update success
				if ($this->EditRow()) { // Update record based on key
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("UpdateSuccess")); // Update success
					$sReturnUrl = $this->getReturnUrl();
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Restore form values if update failed
				}
		}

		// Render the record
		$this->RowType = EW_ROWTYPE_EDIT; // Render as Edit
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->id->FldIsDetailKey)
			$this->id->setFormValue($objForm->GetValue("x_id"));
		if (!$this->account_number->FldIsDetailKey) {
			$this->account_number->setFormValue($objForm->GetValue("x_account_number"));
		}
		if (!$this->phone_number->FldIsDetailKey) {
			$this->phone_number->setFormValue($objForm->GetValue("x_phone_number"));
		}
		if (!$this->routing_number->FldIsDetailKey) {
			$this->routing_number->setFormValue($objForm->GetValue("x_routing_number"));
		}
		if (!$this->bank_name->FldIsDetailKey) {
			$this->bank_name->setFormValue($objForm->GetValue("x_bank_name"));
		}
		if (!$this->account_name->FldIsDetailKey) {
			$this->account_name->setFormValue($objForm->GetValue("x_account_name"));
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadRow();
		$this->id->CurrentValue = $this->id->FormValue;
		$this->account_number->CurrentValue = $this->account_number->FormValue;
		$this->phone_number->CurrentValue = $this->phone_number->FormValue;
		$this->routing_number->CurrentValue = $this->routing_number->FormValue;
		$this->bank_name->CurrentValue = $this->bank_name->FormValue;
		$this->account_name->CurrentValue = $this->account_name->FormValue;
	}

	// Load row based on key values
	function LoadRow() {
		global $conn, $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		global $conn;
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->id->setDbValue($rs->fields('id'));
		$this->account_number->setDbValue($rs->fields('account_number'));
		$this->phone_number->setDbValue($rs->fields('phone_number'));
		$this->routing_number->setDbValue($rs->fields('routing_number'));
		$this->bank_name->setDbValue($rs->fields('bank_name'));
		$this->account_name->setDbValue($rs->fields('account_name'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->id->DbValue = $row['id'];
		$this->account_number->DbValue = $row['account_number'];
		$this->phone_number->DbValue = $row['phone_number'];
		$this->routing_number->DbValue = $row['routing_number'];
		$this->bank_name->DbValue = $row['bank_name'];
		$this->account_name->DbValue = $row['account_name'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $conn, $Security, $Language;
		global $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// id
		// account_number
		// phone_number
		// routing_number
		// bank_name
		// account_name

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

			// id
			$this->id->ViewValue = $this->id->CurrentValue;
			$this->id->ViewCustomAttributes = "";

			// account_number
			$this->account_number->ViewValue = $this->account_number->CurrentValue;
			$this->account_number->ViewCustomAttributes = "";

			// phone_number
			$this->phone_number->ViewValue = $this->phone_number->CurrentValue;
			$this->phone_number->ViewCustomAttributes = "";

			// routing_number
			$this->routing_number->ViewValue = $this->routing_number->CurrentValue;
			$this->routing_number->ViewCustomAttributes = "";

			// bank_name
			$this->bank_name->ViewValue = $this->bank_name->CurrentValue;
			$this->bank_name->ViewCustomAttributes = "";

			// account_name
			$this->account_name->ViewValue = $this->account_name->CurrentValue;
			$this->account_name->ViewCustomAttributes = "";

			// id
			$this->id->LinkCustomAttributes = "";
			$this->id->HrefValue = "";
			$this->id->TooltipValue = "";

			// account_number
			$this->account_number->LinkCustomAttributes = "";
			$this->account_number->HrefValue = "";
			$this->account_number->TooltipValue = "";

			// phone_number
			$this->phone_number->LinkCustomAttributes = "";
			$this->phone_number->HrefValue = "";
			$this->phone_number->TooltipValue = "";

			// routing_number
			$this->routing_number->LinkCustomAttributes = "";
			$this->routing_number->HrefValue = "";
			$this->routing_number->TooltipValue = "";

			// bank_name
			$this->bank_name->LinkCustomAttributes = "";
			$this->bank_name->HrefValue = "";
			$this->bank_name->TooltipValue = "";

			// account_name
			$this->account_name->LinkCustomAttributes = "";
			$this->account_name->HrefValue = "";
			$this->account_name->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_EDIT) { // Edit row

			// id
			$this->id->EditAttrs["class"] = "form-control";
			$this->id->EditCustomAttributes = "";
			$this->id->EditValue = $this->id->CurrentValue;
			$this->id->ViewCustomAttributes = "";

			// account_number
			$this->account_number->EditAttrs["class"] = "form-control";
			$this->account_number->EditCustomAttributes = "";
			$this->account_number->EditValue = ew_HtmlEncode($this->account_number->CurrentValue);
			$this->account_number->PlaceHolder = ew_RemoveHtml($this->account_number->FldCaption());

			// phone_number
			$this->phone_number->EditAttrs["class"] = "form-control";
			$this->phone_number->EditCustomAttributes = "";
			$this->phone_number->EditValue = ew_HtmlEncode($this->phone_number->CurrentValue);
			$this->phone_number->PlaceHolder = ew_RemoveHtml($this->phone_number->FldCaption());

			// routing_number
			$this->routing_number->EditAttrs["class"] = "form-control";
			$this->routing_number->EditCustomAttributes = "";
			$this->routing_number->EditValue = ew_HtmlEncode($this->routing_number->CurrentValue);
			$this->routing_number->PlaceHolder = ew_RemoveHtml($this->routing_number->FldCaption());

			// bank_name
			$this->bank_name->EditAttrs["class"] = "form-control";
			$this->bank_name->EditCustomAttributes = "";
			$this->bank_name->EditValue = ew_HtmlEncode($this->bank_name->CurrentValue);
			$this->bank_name->PlaceHolder = ew_RemoveHtml($this->bank_name->FldCaption());

			// account_name
			$this->account_name->EditAttrs["class"] = "form-control";
			$this->account_name->EditCustomAttributes = "";
			$this->account_name->EditValue = ew_HtmlEncode($this->account_name->CurrentValue);
			$this->account_name->PlaceHolder = ew_RemoveHtml($this->account_name->FldCaption());

			// Edit refer script
			// id

			$this->id->HrefValue = "";

			// account_number
			$this->account_number->HrefValue = "";

			// phone_number
			$this->phone_number->HrefValue = "";

			// routing_number
			$this->routing_number->HrefValue = "";

			// bank_name
			$this->bank_name->HrefValue = "";

			// account_name
			$this->account_name->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->account_number->FldIsDetailKey && !is_null($this->account_number->FormValue) && $this->account_number->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->account_number->FldCaption(), $this->account_number->ReqErrMsg));
		}
		if (!$this->phone_number->FldIsDetailKey && !is_null($this->phone_number->FormValue) && $this->phone_number->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->phone_number->FldCaption(), $this->phone_number->ReqErrMsg));
		}
		if (!$this->routing_number->FldIsDetailKey && !is_null($this->routing_number->FormValue) && $this->routing_number->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->routing_number->FldCaption(), $this->routing_number->ReqErrMsg));
		}
		if (!$this->bank_name->FldIsDetailKey && !is_null($this->bank_name->FormValue) && $this->bank_name->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->bank_name->FldCaption(), $this->bank_name->ReqErrMsg));
		}
		if (!$this->account_name->FldIsDetailKey && !is_null($this->account_name->FormValue) && $this->account_name->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->account_name->FldCaption(), $this->account_name->ReqErrMsg));
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Update record based on key values
	function EditRow() {
		global $conn, $Security, $Language;
		$sFilter = $this->KeyFilter();
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE)
			return FALSE;
		if ($rs->EOF) {
			$EditRow = FALSE; // Update Failed
		} else {

			// Save old values
			$rsold = &$rs->fields;
			$this->LoadDbValues($rsold);
			$rsnew = array();

			// account_number
			$this->account_number->SetDbValueDef($rsnew, $this->account_number->CurrentValue, "", $this->account_number->ReadOnly);

			// phone_number
			$this->phone_number->SetDbValueDef($rsnew, $this->phone_number->CurrentValue, "", $this->phone_number->ReadOnly);

			// routing_number
			$this->routing_number->SetDbValueDef($rsnew, $this->routing_number->CurrentValue, "", $this->routing_number->ReadOnly);

			// bank_name
			$this->bank_name->SetDbValueDef($rsnew, $this->bank_name->CurrentValue, "", $this->bank_name->ReadOnly);

			// account_name
			$this->account_name->SetDbValueDef($rsnew, $this->account_name->CurrentValue, "", $this->account_name->ReadOnly);

			// Call Row Updating event
			$bUpdateRow = $this->Row_Updating($rsold, $rsnew);
			if ($bUpdateRow) {
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				if (count($rsnew) > 0)
					$EditRow = $this->Update($rsnew, "", $rsold);
				else
					$EditRow = TRUE; // No field to update
				$conn->raiseErrorFn = '';
				if ($EditRow) {
				}
			} else {
				if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

					// Use the message, do nothing
				} elseif ($this->CancelMessage <> "") {
					$this->setFailureMessage($this->CancelMessage);
					$this->CancelMessage = "";
				} else {
					$this->setFailureMessage($Language->Phrase("UpdateCancelled"));
				}
				$EditRow = FALSE;
			}
		}

		// Call Row_Updated event
		if ($EditRow)
			$this->Row_Updated($rsold, $rsnew);
		$rs->Close();
		return $EditRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "beneficiaryProfilelist.php", "", $this->TableVar, TRUE);
		$PageId = "edit";
		$Breadcrumb->Add("edit", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($beneficiaryProfile_edit)) $beneficiaryProfile_edit = new cbeneficiaryProfile_edit();

// Page init
$beneficiaryProfile_edit->Page_Init();

// Page main
$beneficiaryProfile_edit->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$beneficiaryProfile_edit->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Page object
var beneficiaryProfile_edit = new ew_Page("beneficiaryProfile_edit");
beneficiaryProfile_edit.PageID = "edit"; // Page ID
var EW_PAGE_ID = beneficiaryProfile_edit.PageID; // For backward compatibility

// Form object
var fbeneficiaryProfileedit = new ew_Form("fbeneficiaryProfileedit");

// Validate form
fbeneficiaryProfileedit.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	this.PostAutoSuggest();
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_account_number");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $beneficiaryProfile->account_number->FldCaption(), $beneficiaryProfile->account_number->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_phone_number");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $beneficiaryProfile->phone_number->FldCaption(), $beneficiaryProfile->phone_number->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_routing_number");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $beneficiaryProfile->routing_number->FldCaption(), $beneficiaryProfile->routing_number->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_bank_name");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $beneficiaryProfile->bank_name->FldCaption(), $beneficiaryProfile->bank_name->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_account_name");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $beneficiaryProfile->account_name->FldCaption(), $beneficiaryProfile->account_name->ReqErrMsg)) ?>");

			// Set up row object
			ew_ElementsToRow(fobj);

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
fbeneficiaryProfileedit.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fbeneficiaryProfileedit.ValidateRequired = true;
<?php } else { ?>
fbeneficiaryProfileedit.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
// Form object for search

</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $beneficiaryProfile_edit->ShowPageHeader(); ?>
<?php
$beneficiaryProfile_edit->ShowMessage();
?>
<form name="fbeneficiaryProfileedit" id="fbeneficiaryProfileedit" class="form-horizontal ewForm ewEditForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($beneficiaryProfile_edit->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $beneficiaryProfile_edit->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="beneficiaryProfile">
<input type="hidden" name="a_edit" id="a_edit" value="U">
<div>
<?php if ($beneficiaryProfile->id->Visible) { // id ?>
	<div id="r_id" class="form-group">
		<label id="elh_beneficiaryProfile_id" class="col-sm-2 control-label ewLabel"><?php echo $beneficiaryProfile->id->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $beneficiaryProfile->id->CellAttributes() ?>>
<span id="el_beneficiaryProfile_id">
<span<?php echo $beneficiaryProfile->id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $beneficiaryProfile->id->EditValue ?></p></span>
</span>
<input type="hidden" data-field="x_id" name="x_id" id="x_id" value="<?php echo ew_HtmlEncode($beneficiaryProfile->id->CurrentValue) ?>">
<?php echo $beneficiaryProfile->id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($beneficiaryProfile->account_number->Visible) { // account_number ?>
	<div id="r_account_number" class="form-group">
		<label id="elh_beneficiaryProfile_account_number" for="x_account_number" class="col-sm-2 control-label ewLabel"><?php echo $beneficiaryProfile->account_number->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $beneficiaryProfile->account_number->CellAttributes() ?>>
<span id="el_beneficiaryProfile_account_number">
<input type="text" data-field="x_account_number" name="x_account_number" id="x_account_number" size="30" maxlength="10" placeholder="<?php echo ew_HtmlEncode($beneficiaryProfile->account_number->PlaceHolder) ?>" value="<?php echo $beneficiaryProfile->account_number->EditValue ?>"<?php echo $beneficiaryProfile->account_number->EditAttributes() ?>>
</span>
<?php echo $beneficiaryProfile->account_number->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($beneficiaryProfile->phone_number->Visible) { // phone_number ?>
	<div id="r_phone_number" class="form-group">
		<label id="elh_beneficiaryProfile_phone_number" for="x_phone_number" class="col-sm-2 control-label ewLabel"><?php echo $beneficiaryProfile->phone_number->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $beneficiaryProfile->phone_number->CellAttributes() ?>>
<span id="el_beneficiaryProfile_phone_number">
<input type="text" data-field="x_phone_number" name="x_phone_number" id="x_phone_number" size="30" maxlength="13" placeholder="<?php echo ew_HtmlEncode($beneficiaryProfile->phone_number->PlaceHolder) ?>" value="<?php echo $beneficiaryProfile->phone_number->EditValue ?>"<?php echo $beneficiaryProfile->phone_number->EditAttributes() ?>>
</span>
<?php echo $beneficiaryProfile->phone_number->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($beneficiaryProfile->routing_number->Visible) { // routing_number ?>
	<div id="r_routing_number" class="form-group">
		<label id="elh_beneficiaryProfile_routing_number" for="x_routing_number" class="col-sm-2 control-label ewLabel"><?php echo $beneficiaryProfile->routing_number->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $beneficiaryProfile->routing_number->CellAttributes() ?>>
<span id="el_beneficiaryProfile_routing_number">
<input type="text" data-field="x_routing_number" name="x_routing_number" id="x_routing_number" size="30" maxlength="12" placeholder="<?php echo ew_HtmlEncode($beneficiaryProfile->routing_number->PlaceHolder) ?>" value="<?php echo $beneficiaryProfile->routing_number->EditValue ?>"<?php echo $beneficiaryProfile->routing_number->EditAttributes() ?>>
</span>
<?php echo $beneficiaryProfile->routing_number->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($beneficiaryProfile->bank_name->Visible) { // bank_name ?>
	<div id="r_bank_name" class="form-group">
		<label id="elh_beneficiaryProfile_bank_name" for="x_bank_name" class="col-sm-2 control-label ewLabel"><?php echo $beneficiaryProfile->bank_name->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $beneficiaryProfile->bank_name->CellAttributes() ?>>
<span id="el_beneficiaryProfile_bank_name">
<input type="text" data-field="x_bank_name" name="x_bank_name" id="x_bank_name" size="30" maxlength="100" placeholder="<?php echo ew_HtmlEncode($beneficiaryProfile->bank_name->PlaceHolder) ?>" value="<?php echo $beneficiaryProfile->bank_name->EditValue ?>"<?php echo $beneficiaryProfile->bank_name->EditAttributes() ?>>
</span>
<?php echo $beneficiaryProfile->bank_name->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($beneficiaryProfile->account_name->Visible) { // account_name ?>
	<div id="r_account_name" class="form-group">
		<label id="elh_beneficiaryProfile_account_name" for="x_account_name" class="col-sm-2 control-label ewLabel"><?php echo $beneficiaryProfile->account_name->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $beneficiaryProfile->account_name->CellAttributes() ?>>
<span id="el_beneficiaryProfile_account_name">
<input type="text" data-field="x_account_name" name="x_account_name" id="x_account_name" size="30" maxlength="100" placeholder="<?php echo ew_HtmlEncode($beneficiaryProfile->account_name->PlaceHolder) ?>" value="<?php echo $beneficiaryProfile->account_name->EditValue ?>"<?php echo $beneficiaryProfile->account_name->EditAttributes() ?>>
</span>
<?php echo $beneficiaryProfile->account_name->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("SaveBtn") ?></button>
	</div>
</div>
</form>
<script type="text/javascript">
fbeneficiaryProfileedit.Init();
</script>
<?php
$beneficiaryProfile_edit->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$beneficiaryProfile_edit->Page_Terminate();
?>
