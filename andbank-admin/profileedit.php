<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg11.php" ?>
<?php include_once "ewmysql11.php" ?>
<?php include_once "phpfn11.php" ?>
<?php include_once "profileinfo.php" ?>
<?php include_once "userfn11.php" ?>
<?php

//
// Page class
//

$profile_edit = NULL; // Initialize page object first

class cprofile_edit extends cprofile {

	// Page ID
	var $PageID = 'edit';

	// Project ID
	var $ProjectID = "{270D70C4-A473-4EEA-B457-A25A3D8EB6E2}";

	// Table name
	var $TableName = 'profile';

	// Page object name
	var $PageObjName = 'profile_edit';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME]);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		$GLOBALS["Page"] = &$this;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (profile)
		if (!isset($GLOBALS["profile"]) || get_class($GLOBALS["profile"]) == "cprofile") {
			$GLOBALS["profile"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["profile"];
		}

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'edit', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'profile', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect();
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if (!$Security->IsLoggedIn()) {
			$Security->SaveLastUrl();
			$this->Page_Terminate(ew_GetUrl("login.php"));
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $conn, $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $profile;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($profile);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		$conn->Close();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $DbMasterFilter;
	var $DbDetailFilter;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;

		// Load key from QueryString
		if (@$_GET["id"] <> "") {
			$this->id->setQueryStringValue($_GET["id"]);
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Process form if post back
		if (@$_POST["a_edit"] <> "") {
			$this->CurrentAction = $_POST["a_edit"]; // Get action code
			$this->LoadFormValues(); // Get form values
		} else {
			$this->CurrentAction = "I"; // Default action is display
		}

		// Check if valid key
		if ($this->id->CurrentValue == "")
			$this->Page_Terminate("profilelist.php"); // Invalid key, return to list

		// Validate form if post back
		if (@$_POST["a_edit"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = ""; // Form error, reset action
				$this->setFailureMessage($gsFormError);
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues();
			}
		}
		switch ($this->CurrentAction) {
			case "I": // Get a record to display
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("profilelist.php"); // No matching record, return to list
				}
				break;
			Case "U": // Update
				$this->SendEmail = TRUE; // Send email on update success
				if ($this->EditRow()) { // Update record based on key
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("UpdateSuccess")); // Update success
					$sReturnUrl = $this->getReturnUrl();
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Restore form values if update failed
				}
		}

		// Render the record
		$this->RowType = EW_ROWTYPE_EDIT; // Render as Edit
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->id->FldIsDetailKey)
			$this->id->setFormValue($objForm->GetValue("x_id"));
		if (!$this->firstname->FldIsDetailKey) {
			$this->firstname->setFormValue($objForm->GetValue("x_firstname"));
		}
		if (!$this->lastname->FldIsDetailKey) {
			$this->lastname->setFormValue($objForm->GetValue("x_lastname"));
		}
		if (!$this->location->FldIsDetailKey) {
			$this->location->setFormValue($objForm->GetValue("x_location"));
		}
		if (!$this->user->FldIsDetailKey) {
			$this->user->setFormValue($objForm->GetValue("x_user"));
		}
		if (!$this->pass->FldIsDetailKey) {
			$this->pass->setFormValue($objForm->GetValue("x_pass"));
		}
		if (!$this->_email->FldIsDetailKey) {
			$this->_email->setFormValue($objForm->GetValue("x__email"));
		}
		if (!$this->a_balance->FldIsDetailKey) {
			$this->a_balance->setFormValue($objForm->GetValue("x_a_balance"));
		}
		if (!$this->c_balance->FldIsDetailKey) {
			$this->c_balance->setFormValue($objForm->GetValue("x_c_balance"));
		}
		if (!$this->ac_no->FldIsDetailKey) {
			$this->ac_no->setFormValue($objForm->GetValue("x_ac_no"));
		}
		if (!$this->pin->FldIsDetailKey) {
			$this->pin->setFormValue($objForm->GetValue("x_pin"));
		}
		if (!$this->image->FldIsDetailKey) {
			$this->image->setFormValue($objForm->GetValue("x_image"));
		}
		if (!$this->currency->FldIsDetailKey) {
			$this->currency->setFormValue($objForm->GetValue("x_currency"));
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadRow();
		$this->id->CurrentValue = $this->id->FormValue;
		$this->firstname->CurrentValue = $this->firstname->FormValue;
		$this->lastname->CurrentValue = $this->lastname->FormValue;
		$this->location->CurrentValue = $this->location->FormValue;
		$this->user->CurrentValue = $this->user->FormValue;
		$this->pass->CurrentValue = $this->pass->FormValue;
		$this->_email->CurrentValue = $this->_email->FormValue;
		$this->a_balance->CurrentValue = $this->a_balance->FormValue;
		$this->c_balance->CurrentValue = $this->c_balance->FormValue;
		$this->ac_no->CurrentValue = $this->ac_no->FormValue;
		$this->pin->CurrentValue = $this->pin->FormValue;
		$this->image->CurrentValue = $this->image->FormValue;
		$this->currency->CurrentValue = $this->currency->FormValue;
	}

	// Load row based on key values
	function LoadRow() {
		global $conn, $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		global $conn;
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->id->setDbValue($rs->fields('id'));
		$this->firstname->setDbValue($rs->fields('firstname'));
		$this->lastname->setDbValue($rs->fields('lastname'));
		$this->location->setDbValue($rs->fields('location'));
		$this->user->setDbValue($rs->fields('user'));
		$this->pass->setDbValue($rs->fields('pass'));
		$this->_email->setDbValue($rs->fields('email'));
		$this->a_balance->setDbValue($rs->fields('a_balance'));
		$this->c_balance->setDbValue($rs->fields('c_balance'));
		$this->ac_no->setDbValue($rs->fields('ac_no'));
		$this->pin->setDbValue($rs->fields('pin'));
		$this->image->setDbValue($rs->fields('image'));
		$this->currency->setDbValue($rs->fields('currency'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->id->DbValue = $row['id'];
		$this->firstname->DbValue = $row['firstname'];
		$this->lastname->DbValue = $row['lastname'];
		$this->location->DbValue = $row['location'];
		$this->user->DbValue = $row['user'];
		$this->pass->DbValue = $row['pass'];
		$this->_email->DbValue = $row['email'];
		$this->a_balance->DbValue = $row['a_balance'];
		$this->c_balance->DbValue = $row['c_balance'];
		$this->ac_no->DbValue = $row['ac_no'];
		$this->pin->DbValue = $row['pin'];
		$this->image->DbValue = $row['image'];
		$this->currency->DbValue = $row['currency'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $conn, $Security, $Language;
		global $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// id
		// firstname
		// lastname
		// location
		// user
		// pass
		// email
		// a_balance
		// c_balance
		// ac_no
		// pin
		// image
		// currency

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

			// id
			$this->id->ViewValue = $this->id->CurrentValue;
			$this->id->ViewCustomAttributes = "";

			// firstname
			$this->firstname->ViewValue = $this->firstname->CurrentValue;
			$this->firstname->ViewCustomAttributes = "";

			// lastname
			$this->lastname->ViewValue = $this->lastname->CurrentValue;
			$this->lastname->ViewCustomAttributes = "";

			// location
			$this->location->ViewValue = $this->location->CurrentValue;
			$this->location->ViewCustomAttributes = "";

			// user
			$this->user->ViewValue = $this->user->CurrentValue;
			$this->user->ViewCustomAttributes = "";

			// pass
			$this->pass->ViewValue = $this->pass->CurrentValue;
			$this->pass->ViewCustomAttributes = "";

			// email
			$this->_email->ViewValue = $this->_email->CurrentValue;
			$this->_email->ViewCustomAttributes = "";

			// a_balance
			$this->a_balance->ViewValue = $this->a_balance->CurrentValue;
			$this->a_balance->ViewCustomAttributes = "";

			// c_balance
			$this->c_balance->ViewValue = $this->c_balance->CurrentValue;
			$this->c_balance->ViewCustomAttributes = "";

			// ac_no
			$this->ac_no->ViewValue = $this->ac_no->CurrentValue;
			$this->ac_no->ViewCustomAttributes = "";

			// pin
			$this->pin->ViewValue = $this->pin->CurrentValue;
			$this->pin->ViewCustomAttributes = "";

			// image
			$this->image->ViewValue = $this->image->CurrentValue;
			$this->image->ViewCustomAttributes = "";

			// currency
			$this->currency->ViewValue = $this->currency->CurrentValue;
			$this->currency->ViewCustomAttributes = "";

			// id
			$this->id->LinkCustomAttributes = "";
			$this->id->HrefValue = "";
			$this->id->TooltipValue = "";

			// firstname
			$this->firstname->LinkCustomAttributes = "";
			$this->firstname->HrefValue = "";
			$this->firstname->TooltipValue = "";

			// lastname
			$this->lastname->LinkCustomAttributes = "";
			$this->lastname->HrefValue = "";
			$this->lastname->TooltipValue = "";

			// location
			$this->location->LinkCustomAttributes = "";
			$this->location->HrefValue = "";
			$this->location->TooltipValue = "";

			// user
			$this->user->LinkCustomAttributes = "";
			$this->user->HrefValue = "";
			$this->user->TooltipValue = "";

			// pass
			$this->pass->LinkCustomAttributes = "";
			$this->pass->HrefValue = "";
			$this->pass->TooltipValue = "";

			// email
			$this->_email->LinkCustomAttributes = "";
			$this->_email->HrefValue = "";
			$this->_email->TooltipValue = "";

			// a_balance
			$this->a_balance->LinkCustomAttributes = "";
			$this->a_balance->HrefValue = "";
			$this->a_balance->TooltipValue = "";

			// c_balance
			$this->c_balance->LinkCustomAttributes = "";
			$this->c_balance->HrefValue = "";
			$this->c_balance->TooltipValue = "";

			// ac_no
			$this->ac_no->LinkCustomAttributes = "";
			$this->ac_no->HrefValue = "";
			$this->ac_no->TooltipValue = "";

			// pin
			$this->pin->LinkCustomAttributes = "";
			$this->pin->HrefValue = "";
			$this->pin->TooltipValue = "";

			// image
			$this->image->LinkCustomAttributes = "";
			$this->image->HrefValue = "";
			$this->image->TooltipValue = "";

			// currency
			$this->currency->LinkCustomAttributes = "";
			$this->currency->HrefValue = "";
			$this->currency->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_EDIT) { // Edit row

			// id
			$this->id->EditAttrs["class"] = "form-control";
			$this->id->EditCustomAttributes = "";
			$this->id->EditValue = $this->id->CurrentValue;
			$this->id->ViewCustomAttributes = "";

			// firstname
			$this->firstname->EditAttrs["class"] = "form-control";
			$this->firstname->EditCustomAttributes = "";
			$this->firstname->EditValue = ew_HtmlEncode($this->firstname->CurrentValue);
			$this->firstname->PlaceHolder = ew_RemoveHtml($this->firstname->FldCaption());

			// lastname
			$this->lastname->EditAttrs["class"] = "form-control";
			$this->lastname->EditCustomAttributes = "";
			$this->lastname->EditValue = ew_HtmlEncode($this->lastname->CurrentValue);
			$this->lastname->PlaceHolder = ew_RemoveHtml($this->lastname->FldCaption());

			// location
			$this->location->EditAttrs["class"] = "form-control";
			$this->location->EditCustomAttributes = "";
			$this->location->EditValue = ew_HtmlEncode($this->location->CurrentValue);
			$this->location->PlaceHolder = ew_RemoveHtml($this->location->FldCaption());

			// user
			$this->user->EditAttrs["class"] = "form-control";
			$this->user->EditCustomAttributes = "";
			$this->user->EditValue = ew_HtmlEncode($this->user->CurrentValue);
			$this->user->PlaceHolder = ew_RemoveHtml($this->user->FldCaption());

			// pass
			$this->pass->EditAttrs["class"] = "form-control";
			$this->pass->EditCustomAttributes = "";
			$this->pass->EditValue = ew_HtmlEncode($this->pass->CurrentValue);
			$this->pass->PlaceHolder = ew_RemoveHtml($this->pass->FldCaption());

			// email
			$this->_email->EditAttrs["class"] = "form-control";
			$this->_email->EditCustomAttributes = "";
			$this->_email->EditValue = ew_HtmlEncode($this->_email->CurrentValue);
			$this->_email->PlaceHolder = ew_RemoveHtml($this->_email->FldCaption());

			// a_balance
			$this->a_balance->EditAttrs["class"] = "form-control";
			$this->a_balance->EditCustomAttributes = "";
			$this->a_balance->EditValue = ew_HtmlEncode($this->a_balance->CurrentValue);
			$this->a_balance->PlaceHolder = ew_RemoveHtml($this->a_balance->FldCaption());

			// c_balance
			$this->c_balance->EditAttrs["class"] = "form-control";
			$this->c_balance->EditCustomAttributes = "";
			$this->c_balance->EditValue = ew_HtmlEncode($this->c_balance->CurrentValue);
			$this->c_balance->PlaceHolder = ew_RemoveHtml($this->c_balance->FldCaption());

			// ac_no
			$this->ac_no->EditAttrs["class"] = "form-control";
			$this->ac_no->EditCustomAttributes = "";
			$this->ac_no->EditValue = ew_HtmlEncode($this->ac_no->CurrentValue);
			$this->ac_no->PlaceHolder = ew_RemoveHtml($this->ac_no->FldCaption());

			// pin
			$this->pin->EditAttrs["class"] = "form-control";
			$this->pin->EditCustomAttributes = "";
			$this->pin->EditValue = ew_HtmlEncode($this->pin->CurrentValue);
			$this->pin->PlaceHolder = ew_RemoveHtml($this->pin->FldCaption());

			// image
			$this->image->EditAttrs["class"] = "form-control";
			$this->image->EditCustomAttributes = "";
			$this->image->EditValue = ew_HtmlEncode($this->image->CurrentValue);
			$this->image->PlaceHolder = ew_RemoveHtml($this->image->FldCaption());

			// currency
			$this->currency->EditAttrs["class"] = "form-control";
			$this->currency->EditCustomAttributes = "";
			$this->currency->EditValue = ew_HtmlEncode($this->currency->CurrentValue);
			$this->currency->PlaceHolder = ew_RemoveHtml($this->currency->FldCaption());

			// Edit refer script
			// id

			$this->id->HrefValue = "";

			// firstname
			$this->firstname->HrefValue = "";

			// lastname
			$this->lastname->HrefValue = "";

			// location
			$this->location->HrefValue = "";

			// user
			$this->user->HrefValue = "";

			// pass
			$this->pass->HrefValue = "";

			// email
			$this->_email->HrefValue = "";

			// a_balance
			$this->a_balance->HrefValue = "";

			// c_balance
			$this->c_balance->HrefValue = "";

			// ac_no
			$this->ac_no->HrefValue = "";

			// pin
			$this->pin->HrefValue = "";

			// image
			$this->image->HrefValue = "";

			// currency
			$this->currency->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->firstname->FldIsDetailKey && !is_null($this->firstname->FormValue) && $this->firstname->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->firstname->FldCaption(), $this->firstname->ReqErrMsg));
		}
		if (!$this->lastname->FldIsDetailKey && !is_null($this->lastname->FormValue) && $this->lastname->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->lastname->FldCaption(), $this->lastname->ReqErrMsg));
		}
		if (!$this->location->FldIsDetailKey && !is_null($this->location->FormValue) && $this->location->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->location->FldCaption(), $this->location->ReqErrMsg));
		}
		if (!$this->user->FldIsDetailKey && !is_null($this->user->FormValue) && $this->user->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->user->FldCaption(), $this->user->ReqErrMsg));
		}
		if (!$this->pass->FldIsDetailKey && !is_null($this->pass->FormValue) && $this->pass->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->pass->FldCaption(), $this->pass->ReqErrMsg));
		}
		if (!$this->_email->FldIsDetailKey && !is_null($this->_email->FormValue) && $this->_email->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->_email->FldCaption(), $this->_email->ReqErrMsg));
		}
		if (!$this->a_balance->FldIsDetailKey && !is_null($this->a_balance->FormValue) && $this->a_balance->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->a_balance->FldCaption(), $this->a_balance->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->a_balance->FormValue)) {
			ew_AddMessage($gsFormError, $this->a_balance->FldErrMsg());
		}
		if (!$this->c_balance->FldIsDetailKey && !is_null($this->c_balance->FormValue) && $this->c_balance->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->c_balance->FldCaption(), $this->c_balance->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->c_balance->FormValue)) {
			ew_AddMessage($gsFormError, $this->c_balance->FldErrMsg());
		}
		if (!$this->ac_no->FldIsDetailKey && !is_null($this->ac_no->FormValue) && $this->ac_no->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->ac_no->FldCaption(), $this->ac_no->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->ac_no->FormValue)) {
			ew_AddMessage($gsFormError, $this->ac_no->FldErrMsg());
		}
		if (!$this->pin->FldIsDetailKey && !is_null($this->pin->FormValue) && $this->pin->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->pin->FldCaption(), $this->pin->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->pin->FormValue)) {
			ew_AddMessage($gsFormError, $this->pin->FldErrMsg());
		}
		if (!$this->image->FldIsDetailKey && !is_null($this->image->FormValue) && $this->image->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->image->FldCaption(), $this->image->ReqErrMsg));
		}
		if (!$this->currency->FldIsDetailKey && !is_null($this->currency->FormValue) && $this->currency->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->currency->FldCaption(), $this->currency->ReqErrMsg));
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Update record based on key values
	function EditRow() {
		global $conn, $Security, $Language;
		$sFilter = $this->KeyFilter();
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE)
			return FALSE;
		if ($rs->EOF) {
			$EditRow = FALSE; // Update Failed
		} else {

			// Save old values
			$rsold = &$rs->fields;
			$this->LoadDbValues($rsold);
			$rsnew = array();

			// firstname
			$this->firstname->SetDbValueDef($rsnew, $this->firstname->CurrentValue, "", $this->firstname->ReadOnly);

			// lastname
			$this->lastname->SetDbValueDef($rsnew, $this->lastname->CurrentValue, "", $this->lastname->ReadOnly);

			// location
			$this->location->SetDbValueDef($rsnew, $this->location->CurrentValue, "", $this->location->ReadOnly);

			// user
			$this->user->SetDbValueDef($rsnew, $this->user->CurrentValue, "", $this->user->ReadOnly);

			// pass
			$this->pass->SetDbValueDef($rsnew, $this->pass->CurrentValue, "", $this->pass->ReadOnly);

			// email
			$this->_email->SetDbValueDef($rsnew, $this->_email->CurrentValue, "", $this->_email->ReadOnly);

			// a_balance
			$this->a_balance->SetDbValueDef($rsnew, $this->a_balance->CurrentValue, 0, $this->a_balance->ReadOnly);

			// c_balance
			$this->c_balance->SetDbValueDef($rsnew, $this->c_balance->CurrentValue, 0, $this->c_balance->ReadOnly);

			// ac_no
			$this->ac_no->SetDbValueDef($rsnew, $this->ac_no->CurrentValue, 0, $this->ac_no->ReadOnly);

			// pin
			$this->pin->SetDbValueDef($rsnew, $this->pin->CurrentValue, 0, $this->pin->ReadOnly);

			// image
			$this->image->SetDbValueDef($rsnew, $this->image->CurrentValue, "", $this->image->ReadOnly);

			// currency
			$this->currency->SetDbValueDef($rsnew, $this->currency->CurrentValue, "", $this->currency->ReadOnly);

			// Call Row Updating event
			$bUpdateRow = $this->Row_Updating($rsold, $rsnew);
			if ($bUpdateRow) {
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				if (count($rsnew) > 0)
					$EditRow = $this->Update($rsnew, "", $rsold);
				else
					$EditRow = TRUE; // No field to update
				$conn->raiseErrorFn = '';
				if ($EditRow) {
				}
			} else {
				if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

					// Use the message, do nothing
				} elseif ($this->CancelMessage <> "") {
					$this->setFailureMessage($this->CancelMessage);
					$this->CancelMessage = "";
				} else {
					$this->setFailureMessage($Language->Phrase("UpdateCancelled"));
				}
				$EditRow = FALSE;
			}
		}

		// Call Row_Updated event
		if ($EditRow)
			$this->Row_Updated($rsold, $rsnew);
		$rs->Close();
		return $EditRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "profilelist.php", "", $this->TableVar, TRUE);
		$PageId = "edit";
		$Breadcrumb->Add("edit", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($profile_edit)) $profile_edit = new cprofile_edit();

// Page init
$profile_edit->Page_Init();

// Page main
$profile_edit->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$profile_edit->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Page object
var profile_edit = new ew_Page("profile_edit");
profile_edit.PageID = "edit"; // Page ID
var EW_PAGE_ID = profile_edit.PageID; // For backward compatibility

// Form object
var fprofileedit = new ew_Form("fprofileedit");

// Validate form
fprofileedit.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	this.PostAutoSuggest();
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_firstname");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $profile->firstname->FldCaption(), $profile->firstname->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_lastname");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $profile->lastname->FldCaption(), $profile->lastname->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_location");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $profile->location->FldCaption(), $profile->location->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_user");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $profile->user->FldCaption(), $profile->user->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_pass");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $profile->pass->FldCaption(), $profile->pass->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "__email");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $profile->_email->FldCaption(), $profile->_email->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_a_balance");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $profile->a_balance->FldCaption(), $profile->a_balance->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_a_balance");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($profile->a_balance->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_c_balance");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $profile->c_balance->FldCaption(), $profile->c_balance->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_c_balance");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($profile->c_balance->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_ac_no");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $profile->ac_no->FldCaption(), $profile->ac_no->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_ac_no");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($profile->ac_no->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_pin");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $profile->pin->FldCaption(), $profile->pin->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_pin");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($profile->pin->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_image");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $profile->image->FldCaption(), $profile->image->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_currency");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $profile->currency->FldCaption(), $profile->currency->ReqErrMsg)) ?>");

			// Set up row object
			ew_ElementsToRow(fobj);

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
fprofileedit.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fprofileedit.ValidateRequired = true;
<?php } else { ?>
fprofileedit.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
// Form object for search

</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $profile_edit->ShowPageHeader(); ?>
<?php
$profile_edit->ShowMessage();
?>
<form name="fprofileedit" id="fprofileedit" class="form-horizontal ewForm ewEditForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($profile_edit->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $profile_edit->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="profile">
<input type="hidden" name="a_edit" id="a_edit" value="U">
<div>
<?php if ($profile->id->Visible) { // id ?>
	<div id="r_id" class="form-group">
		<label id="elh_profile_id" class="col-sm-2 control-label ewLabel"><?php echo $profile->id->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $profile->id->CellAttributes() ?>>
<span id="el_profile_id">
<span<?php echo $profile->id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $profile->id->EditValue ?></p></span>
</span>
<input type="hidden" data-field="x_id" name="x_id" id="x_id" value="<?php echo ew_HtmlEncode($profile->id->CurrentValue) ?>">
<?php echo $profile->id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($profile->firstname->Visible) { // firstname ?>
	<div id="r_firstname" class="form-group">
		<label id="elh_profile_firstname" for="x_firstname" class="col-sm-2 control-label ewLabel"><?php echo $profile->firstname->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $profile->firstname->CellAttributes() ?>>
<span id="el_profile_firstname">
<input type="text" data-field="x_firstname" name="x_firstname" id="x_firstname" size="30" maxlength="25" placeholder="<?php echo ew_HtmlEncode($profile->firstname->PlaceHolder) ?>" value="<?php echo $profile->firstname->EditValue ?>"<?php echo $profile->firstname->EditAttributes() ?>>
</span>
<?php echo $profile->firstname->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($profile->lastname->Visible) { // lastname ?>
	<div id="r_lastname" class="form-group">
		<label id="elh_profile_lastname" for="x_lastname" class="col-sm-2 control-label ewLabel"><?php echo $profile->lastname->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $profile->lastname->CellAttributes() ?>>
<span id="el_profile_lastname">
<input type="text" data-field="x_lastname" name="x_lastname" id="x_lastname" size="30" maxlength="25" placeholder="<?php echo ew_HtmlEncode($profile->lastname->PlaceHolder) ?>" value="<?php echo $profile->lastname->EditValue ?>"<?php echo $profile->lastname->EditAttributes() ?>>
</span>
<?php echo $profile->lastname->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($profile->location->Visible) { // location ?>
	<div id="r_location" class="form-group">
		<label id="elh_profile_location" for="x_location" class="col-sm-2 control-label ewLabel"><?php echo $profile->location->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $profile->location->CellAttributes() ?>>
<span id="el_profile_location">
<input type="text" data-field="x_location" name="x_location" id="x_location" size="30" maxlength="25" placeholder="<?php echo ew_HtmlEncode($profile->location->PlaceHolder) ?>" value="<?php echo $profile->location->EditValue ?>"<?php echo $profile->location->EditAttributes() ?>>
</span>
<?php echo $profile->location->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($profile->user->Visible) { // user ?>
	<div id="r_user" class="form-group">
		<label id="elh_profile_user" for="x_user" class="col-sm-2 control-label ewLabel"><?php echo $profile->user->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $profile->user->CellAttributes() ?>>
<span id="el_profile_user">
<input type="text" data-field="x_user" name="x_user" id="x_user" size="30" maxlength="20" placeholder="<?php echo ew_HtmlEncode($profile->user->PlaceHolder) ?>" value="<?php echo $profile->user->EditValue ?>"<?php echo $profile->user->EditAttributes() ?>>
</span>
<?php echo $profile->user->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($profile->pass->Visible) { // pass ?>
	<div id="r_pass" class="form-group">
		<label id="elh_profile_pass" for="x_pass" class="col-sm-2 control-label ewLabel"><?php echo $profile->pass->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $profile->pass->CellAttributes() ?>>
<span id="el_profile_pass">
<input type="text" data-field="x_pass" name="x_pass" id="x_pass" size="30" maxlength="25" placeholder="<?php echo ew_HtmlEncode($profile->pass->PlaceHolder) ?>" value="<?php echo $profile->pass->EditValue ?>"<?php echo $profile->pass->EditAttributes() ?>>
</span>
<?php echo $profile->pass->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($profile->_email->Visible) { // email ?>
	<div id="r__email" class="form-group">
		<label id="elh_profile__email" for="x__email" class="col-sm-2 control-label ewLabel"><?php echo $profile->_email->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $profile->_email->CellAttributes() ?>>
<span id="el_profile__email">
<input type="text" data-field="x__email" name="x__email" id="x__email" size="30" maxlength="30" placeholder="<?php echo ew_HtmlEncode($profile->_email->PlaceHolder) ?>" value="<?php echo $profile->_email->EditValue ?>"<?php echo $profile->_email->EditAttributes() ?>>
</span>
<?php echo $profile->_email->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($profile->a_balance->Visible) { // a_balance ?>
	<div id="r_a_balance" class="form-group">
		<label id="elh_profile_a_balance" for="x_a_balance" class="col-sm-2 control-label ewLabel"><?php echo $profile->a_balance->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $profile->a_balance->CellAttributes() ?>>
<span id="el_profile_a_balance">
<input type="text" data-field="x_a_balance" name="x_a_balance" id="x_a_balance" size="30" placeholder="<?php echo ew_HtmlEncode($profile->a_balance->PlaceHolder) ?>" value="<?php echo $profile->a_balance->EditValue ?>"<?php echo $profile->a_balance->EditAttributes() ?>>
</span>
<?php echo $profile->a_balance->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($profile->c_balance->Visible) { // c_balance ?>
	<div id="r_c_balance" class="form-group">
		<label id="elh_profile_c_balance" for="x_c_balance" class="col-sm-2 control-label ewLabel"><?php echo $profile->c_balance->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $profile->c_balance->CellAttributes() ?>>
<span id="el_profile_c_balance">
<input type="text" data-field="x_c_balance" name="x_c_balance" id="x_c_balance" size="30" placeholder="<?php echo ew_HtmlEncode($profile->c_balance->PlaceHolder) ?>" value="<?php echo $profile->c_balance->EditValue ?>"<?php echo $profile->c_balance->EditAttributes() ?>>
</span>
<?php echo $profile->c_balance->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($profile->ac_no->Visible) { // ac_no ?>
	<div id="r_ac_no" class="form-group">
		<label id="elh_profile_ac_no" for="x_ac_no" class="col-sm-2 control-label ewLabel"><?php echo $profile->ac_no->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $profile->ac_no->CellAttributes() ?>>
<span id="el_profile_ac_no">
<input type="text" data-field="x_ac_no" name="x_ac_no" id="x_ac_no" size="30" placeholder="<?php echo ew_HtmlEncode($profile->ac_no->PlaceHolder) ?>" value="<?php echo $profile->ac_no->EditValue ?>"<?php echo $profile->ac_no->EditAttributes() ?>>
</span>
<?php echo $profile->ac_no->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($profile->pin->Visible) { // pin ?>
	<div id="r_pin" class="form-group">
		<label id="elh_profile_pin" for="x_pin" class="col-sm-2 control-label ewLabel"><?php echo $profile->pin->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $profile->pin->CellAttributes() ?>>
<span id="el_profile_pin">
<input type="text" data-field="x_pin" name="x_pin" id="x_pin" size="30" placeholder="<?php echo ew_HtmlEncode($profile->pin->PlaceHolder) ?>" value="<?php echo $profile->pin->EditValue ?>"<?php echo $profile->pin->EditAttributes() ?>>
</span>
<?php echo $profile->pin->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($profile->image->Visible) { // image ?>
	<div id="r_image" class="form-group">
		<label id="elh_profile_image" for="x_image" class="col-sm-2 control-label ewLabel"><?php echo $profile->image->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $profile->image->CellAttributes() ?>>
<span id="el_profile_image">
<textarea data-field="x_image" name="x_image" id="x_image" cols="35" rows="4" placeholder="<?php echo ew_HtmlEncode($profile->image->PlaceHolder) ?>"<?php echo $profile->image->EditAttributes() ?>><?php echo $profile->image->EditValue ?></textarea>
</span>
<?php echo $profile->image->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($profile->currency->Visible) { // currency ?>
	<div id="r_currency" class="form-group">
		<label id="elh_profile_currency" for="x_currency" class="col-sm-2 control-label ewLabel"><?php echo $profile->currency->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $profile->currency->CellAttributes() ?>>
<span id="el_profile_currency">
<input type="text" data-field="x_currency" name="x_currency" id="x_currency" size="30" maxlength="30" placeholder="<?php echo ew_HtmlEncode($profile->currency->PlaceHolder) ?>" value="<?php echo $profile->currency->EditValue ?>"<?php echo $profile->currency->EditAttributes() ?>>
</span>
<?php echo $profile->currency->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("SaveBtn") ?></button>
	</div>
</div>
</form>
<script type="text/javascript">
fprofileedit.Init();
</script>
<?php
$profile_edit->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$profile_edit->Page_Terminate();
?>
