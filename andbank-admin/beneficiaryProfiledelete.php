<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg11.php" ?>
<?php include_once "ewmysql11.php" ?>
<?php include_once "phpfn11.php" ?>
<?php include_once "beneficiaryProfileinfo.php" ?>
<?php include_once "userfn11.php" ?>
<?php

//
// Page class
//

$beneficiaryProfile_delete = NULL; // Initialize page object first

class cbeneficiaryProfile_delete extends cbeneficiaryProfile {

	// Page ID
	var $PageID = 'delete';

	// Project ID
	var $ProjectID = "{270D70C4-A473-4EEA-B457-A25A3D8EB6E2}";

	// Table name
	var $TableName = 'beneficiaryProfile';

	// Page object name
	var $PageObjName = 'beneficiaryProfile_delete';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME]);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		$GLOBALS["Page"] = &$this;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (beneficiaryProfile)
		if (!isset($GLOBALS["beneficiaryProfile"]) || get_class($GLOBALS["beneficiaryProfile"]) == "cbeneficiaryProfile") {
			$GLOBALS["beneficiaryProfile"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["beneficiaryProfile"];
		}

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'delete', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'beneficiaryProfile', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect();
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if (!$Security->IsLoggedIn()) {
			$Security->SaveLastUrl();
			$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $conn, $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $beneficiaryProfile;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($beneficiaryProfile);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		$conn->Close();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $TotalRecs = 0;
	var $RecCnt;
	var $RecKeys = array();
	var $Recordset;
	var $StartRowCnt = 1;
	var $RowCnt = 0;

	//
	// Page main
	//
	function Page_Main() {
		global $Language;

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Load key parameters
		$this->RecKeys = $this->GetRecordKeys(); // Load record keys
		$sFilter = $this->GetKeyFilter();
		if ($sFilter == "")
			$this->Page_Terminate("beneficiaryProfilelist.php"); // Prevent SQL injection, return to list

		// Set up filter (SQL WHHERE clause) and get return SQL
		// SQL constructor in beneficiaryProfile class, beneficiaryProfileinfo.php

		$this->CurrentFilter = $sFilter;

		// Get action
		if (@$_POST["a_delete"] <> "") {
			$this->CurrentAction = $_POST["a_delete"];
		} else {
			$this->CurrentAction = "I"; // Display record
		}
		switch ($this->CurrentAction) {
			case "D": // Delete
				$this->SendEmail = TRUE; // Send email on delete success
				if ($this->DeleteRows()) { // Delete rows
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("DeleteSuccess")); // Set up success message
					$this->Page_Terminate($this->getReturnUrl()); // Return to caller
				}
		}
	}

// No functions
	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {
		global $conn;

		// Load List page SQL
		$sSql = $this->SelectSQL();

		// Load recordset
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
		$conn->raiseErrorFn = '';

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $conn, $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		global $conn;
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->id->setDbValue($rs->fields('id'));
		$this->account_number->setDbValue($rs->fields('account_number'));
		$this->phone_number->setDbValue($rs->fields('phone_number'));
		$this->routing_number->setDbValue($rs->fields('routing_number'));
		$this->bank_name->setDbValue($rs->fields('bank_name'));
		$this->account_name->setDbValue($rs->fields('account_name'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->id->DbValue = $row['id'];
		$this->account_number->DbValue = $row['account_number'];
		$this->phone_number->DbValue = $row['phone_number'];
		$this->routing_number->DbValue = $row['routing_number'];
		$this->bank_name->DbValue = $row['bank_name'];
		$this->account_name->DbValue = $row['account_name'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $conn, $Security, $Language;
		global $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// id
		// account_number
		// phone_number
		// routing_number
		// bank_name
		// account_name

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

			// id
			$this->id->ViewValue = $this->id->CurrentValue;
			$this->id->ViewCustomAttributes = "";

			// account_number
			$this->account_number->ViewValue = $this->account_number->CurrentValue;
			$this->account_number->ViewCustomAttributes = "";

			// phone_number
			$this->phone_number->ViewValue = $this->phone_number->CurrentValue;
			$this->phone_number->ViewCustomAttributes = "";

			// routing_number
			$this->routing_number->ViewValue = $this->routing_number->CurrentValue;
			$this->routing_number->ViewCustomAttributes = "";

			// bank_name
			$this->bank_name->ViewValue = $this->bank_name->CurrentValue;
			$this->bank_name->ViewCustomAttributes = "";

			// account_name
			$this->account_name->ViewValue = $this->account_name->CurrentValue;
			$this->account_name->ViewCustomAttributes = "";

			// id
			$this->id->LinkCustomAttributes = "";
			$this->id->HrefValue = "";
			$this->id->TooltipValue = "";

			// account_number
			$this->account_number->LinkCustomAttributes = "";
			$this->account_number->HrefValue = "";
			$this->account_number->TooltipValue = "";

			// phone_number
			$this->phone_number->LinkCustomAttributes = "";
			$this->phone_number->HrefValue = "";
			$this->phone_number->TooltipValue = "";

			// routing_number
			$this->routing_number->LinkCustomAttributes = "";
			$this->routing_number->HrefValue = "";
			$this->routing_number->TooltipValue = "";

			// bank_name
			$this->bank_name->LinkCustomAttributes = "";
			$this->bank_name->HrefValue = "";
			$this->bank_name->TooltipValue = "";

			// account_name
			$this->account_name->LinkCustomAttributes = "";
			$this->account_name->HrefValue = "";
			$this->account_name->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	//
	// Delete records based on current filter
	//
	function DeleteRows() {
		global $conn, $Language, $Security;
		$DeleteRows = TRUE;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE) {
			return FALSE;
		} elseif ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
			$rs->Close();
			return FALSE;

		//} else {
		//	$this->LoadRowValues($rs); // Load row values

		}
		$rows = ($rs) ? $rs->GetRows() : array();
		$conn->BeginTrans();

		// Clone old rows
		$rsold = $rows;
		if ($rs)
			$rs->Close();

		// Call row deleting event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$DeleteRows = $this->Row_Deleting($row);
				if (!$DeleteRows) break;
			}
		}
		if ($DeleteRows) {
			$sKey = "";
			foreach ($rsold as $row) {
				$sThisKey = "";
				if ($sThisKey <> "") $sThisKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
				$sThisKey .= $row['id'];
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				$DeleteRows = $this->Delete($row); // Delete
				$conn->raiseErrorFn = '';
				if ($DeleteRows === FALSE)
					break;
				if ($sKey <> "") $sKey .= ", ";
				$sKey .= $sThisKey;
			}
		} else {

			// Set up error message
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("DeleteCancelled"));
			}
		}
		if ($DeleteRows) {
			$conn->CommitTrans(); // Commit the changes
		} else {
			$conn->RollbackTrans(); // Rollback changes
		}

		// Call Row Deleted event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$this->Row_Deleted($row);
			}
		}
		return $DeleteRows;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "beneficiaryProfilelist.php", "", $this->TableVar, TRUE);
		$PageId = "delete";
		$Breadcrumb->Add("delete", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($beneficiaryProfile_delete)) $beneficiaryProfile_delete = new cbeneficiaryProfile_delete();

// Page init
$beneficiaryProfile_delete->Page_Init();

// Page main
$beneficiaryProfile_delete->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$beneficiaryProfile_delete->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Page object
var beneficiaryProfile_delete = new ew_Page("beneficiaryProfile_delete");
beneficiaryProfile_delete.PageID = "delete"; // Page ID
var EW_PAGE_ID = beneficiaryProfile_delete.PageID; // For backward compatibility

// Form object
var fbeneficiaryProfiledelete = new ew_Form("fbeneficiaryProfiledelete");

// Form_CustomValidate event
fbeneficiaryProfiledelete.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fbeneficiaryProfiledelete.ValidateRequired = true;
<?php } else { ?>
fbeneficiaryProfiledelete.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
// Form object for search

</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php

// Load records for display
if ($beneficiaryProfile_delete->Recordset = $beneficiaryProfile_delete->LoadRecordset())
	$beneficiaryProfile_deleteTotalRecs = $beneficiaryProfile_delete->Recordset->RecordCount(); // Get record count
if ($beneficiaryProfile_deleteTotalRecs <= 0) { // No record found, exit
	if ($beneficiaryProfile_delete->Recordset)
		$beneficiaryProfile_delete->Recordset->Close();
	$beneficiaryProfile_delete->Page_Terminate("beneficiaryProfilelist.php"); // Return to list
}
?>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $beneficiaryProfile_delete->ShowPageHeader(); ?>
<?php
$beneficiaryProfile_delete->ShowMessage();
?>
<form name="fbeneficiaryProfiledelete" id="fbeneficiaryProfiledelete" class="form-inline ewForm ewDeleteForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($beneficiaryProfile_delete->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $beneficiaryProfile_delete->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="beneficiaryProfile">
<input type="hidden" name="a_delete" id="a_delete" value="D">
<?php foreach ($beneficiaryProfile_delete->RecKeys as $key) { ?>
<?php $keyvalue = is_array($key) ? implode($EW_COMPOSITE_KEY_SEPARATOR, $key) : $key; ?>
<input type="hidden" name="key_m[]" value="<?php echo ew_HtmlEncode($keyvalue) ?>">
<?php } ?>
<div class="ewGrid">
<div class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<table class="table ewTable">
<?php echo $beneficiaryProfile->TableCustomInnerHtml ?>
	<thead>
	<tr class="ewTableHeader">
<?php if ($beneficiaryProfile->id->Visible) { // id ?>
		<th><span id="elh_beneficiaryProfile_id" class="beneficiaryProfile_id"><?php echo $beneficiaryProfile->id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($beneficiaryProfile->account_number->Visible) { // account_number ?>
		<th><span id="elh_beneficiaryProfile_account_number" class="beneficiaryProfile_account_number"><?php echo $beneficiaryProfile->account_number->FldCaption() ?></span></th>
<?php } ?>
<?php if ($beneficiaryProfile->phone_number->Visible) { // phone_number ?>
		<th><span id="elh_beneficiaryProfile_phone_number" class="beneficiaryProfile_phone_number"><?php echo $beneficiaryProfile->phone_number->FldCaption() ?></span></th>
<?php } ?>
<?php if ($beneficiaryProfile->routing_number->Visible) { // routing_number ?>
		<th><span id="elh_beneficiaryProfile_routing_number" class="beneficiaryProfile_routing_number"><?php echo $beneficiaryProfile->routing_number->FldCaption() ?></span></th>
<?php } ?>
<?php if ($beneficiaryProfile->bank_name->Visible) { // bank_name ?>
		<th><span id="elh_beneficiaryProfile_bank_name" class="beneficiaryProfile_bank_name"><?php echo $beneficiaryProfile->bank_name->FldCaption() ?></span></th>
<?php } ?>
<?php if ($beneficiaryProfile->account_name->Visible) { // account_name ?>
		<th><span id="elh_beneficiaryProfile_account_name" class="beneficiaryProfile_account_name"><?php echo $beneficiaryProfile->account_name->FldCaption() ?></span></th>
<?php } ?>
	</tr>
	</thead>
	<tbody>
<?php
$beneficiaryProfile_delete->RecCnt = 0;
$i = 0;
while (!$beneficiaryProfile_delete->Recordset->EOF) {
	$beneficiaryProfile_delete->RecCnt++;
	$beneficiaryProfile_delete->RowCnt++;

	// Set row properties
	$beneficiaryProfile->ResetAttrs();
	$beneficiaryProfile->RowType = EW_ROWTYPE_VIEW; // View

	// Get the field contents
	$beneficiaryProfile_delete->LoadRowValues($beneficiaryProfile_delete->Recordset);

	// Render row
	$beneficiaryProfile_delete->RenderRow();
?>
	<tr<?php echo $beneficiaryProfile->RowAttributes() ?>>
<?php if ($beneficiaryProfile->id->Visible) { // id ?>
		<td<?php echo $beneficiaryProfile->id->CellAttributes() ?>>
<span id="el<?php echo $beneficiaryProfile_delete->RowCnt ?>_beneficiaryProfile_id" class="beneficiaryProfile_id">
<span<?php echo $beneficiaryProfile->id->ViewAttributes() ?>>
<?php echo $beneficiaryProfile->id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($beneficiaryProfile->account_number->Visible) { // account_number ?>
		<td<?php echo $beneficiaryProfile->account_number->CellAttributes() ?>>
<span id="el<?php echo $beneficiaryProfile_delete->RowCnt ?>_beneficiaryProfile_account_number" class="beneficiaryProfile_account_number">
<span<?php echo $beneficiaryProfile->account_number->ViewAttributes() ?>>
<?php echo $beneficiaryProfile->account_number->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($beneficiaryProfile->phone_number->Visible) { // phone_number ?>
		<td<?php echo $beneficiaryProfile->phone_number->CellAttributes() ?>>
<span id="el<?php echo $beneficiaryProfile_delete->RowCnt ?>_beneficiaryProfile_phone_number" class="beneficiaryProfile_phone_number">
<span<?php echo $beneficiaryProfile->phone_number->ViewAttributes() ?>>
<?php echo $beneficiaryProfile->phone_number->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($beneficiaryProfile->routing_number->Visible) { // routing_number ?>
		<td<?php echo $beneficiaryProfile->routing_number->CellAttributes() ?>>
<span id="el<?php echo $beneficiaryProfile_delete->RowCnt ?>_beneficiaryProfile_routing_number" class="beneficiaryProfile_routing_number">
<span<?php echo $beneficiaryProfile->routing_number->ViewAttributes() ?>>
<?php echo $beneficiaryProfile->routing_number->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($beneficiaryProfile->bank_name->Visible) { // bank_name ?>
		<td<?php echo $beneficiaryProfile->bank_name->CellAttributes() ?>>
<span id="el<?php echo $beneficiaryProfile_delete->RowCnt ?>_beneficiaryProfile_bank_name" class="beneficiaryProfile_bank_name">
<span<?php echo $beneficiaryProfile->bank_name->ViewAttributes() ?>>
<?php echo $beneficiaryProfile->bank_name->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($beneficiaryProfile->account_name->Visible) { // account_name ?>
		<td<?php echo $beneficiaryProfile->account_name->CellAttributes() ?>>
<span id="el<?php echo $beneficiaryProfile_delete->RowCnt ?>_beneficiaryProfile_account_name" class="beneficiaryProfile_account_name">
<span<?php echo $beneficiaryProfile->account_name->ViewAttributes() ?>>
<?php echo $beneficiaryProfile->account_name->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
	</tr>
<?php
	$beneficiaryProfile_delete->Recordset->MoveNext();
}
$beneficiaryProfile_delete->Recordset->Close();
?>
</tbody>
</table>
</div>
</div>
<div class="btn-group ewButtonGroup">
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("DeleteBtn") ?></button>
</div>
</form>
<script type="text/javascript">
fbeneficiaryProfiledelete.Init();
</script>
<?php
$beneficiaryProfile_delete->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$beneficiaryProfile_delete->Page_Terminate();
?>
