<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg10.php" ?>
<?php include_once "ewmysql10.php" ?>
<?php include_once "phpfn10.php" ?>
<?php include_once "profileinfo.php" ?>
<?php include_once "admin_usersinfo.php" ?>
<?php include_once "userfn10.php" ?>
<?php

//
// Page class
//

$profile_edit = NULL; // Initialize page object first

class cprofile_edit extends cprofile {

	// Page ID
	var $PageID = 'edit';

	// Project ID
	var $ProjectID = "{830E57EC-9344-4D0E-802B-B652CCAA29BD}";

	// Table name
	var $TableName = 'profile';

	// Page object name
	var $PageObjName = 'profile_edit';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-error ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<table class=\"ewStdTable\"><tr><td><div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div></td></tr></table>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language, $UserAgent;

		// User agent
		$UserAgent = ew_UserAgent();
		$GLOBALS["Page"] = &$this;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (profile)
		if (!isset($GLOBALS["profile"])) {
			$GLOBALS["profile"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["profile"];
		}

		// Table object (admin_users)
		if (!isset($GLOBALS['admin_users'])) $GLOBALS['admin_users'] = new cadmin_users();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'edit', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'profile', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect();
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if (!$Security->IsLoggedIn()) {
			$Security->SaveLastUrl();
			$this->Page_Terminate("login.php");
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up curent action
		$this->id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $conn;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();
		$this->Page_Redirecting($url);

		 // Close connection
		$conn->Close();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $DbMasterFilter;
	var $DbDetailFilter;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;

		// Load key from QueryString
		if (@$_GET["id"] <> "") {
			$this->id->setQueryStringValue($_GET["id"]);
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Process form if post back
		if (@$_POST["a_edit"] <> "") {
			$this->CurrentAction = $_POST["a_edit"]; // Get action code
			$this->LoadFormValues(); // Get form values
		} else {
			$this->CurrentAction = "I"; // Default action is display
		}

		// Check if valid key
		if ($this->id->CurrentValue == "")
			$this->Page_Terminate("profilelist.php"); // Invalid key, return to list

		// Validate form if post back
		if (@$_POST["a_edit"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = ""; // Form error, reset action
				$this->setFailureMessage($gsFormError);
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues();
			}
		}
		switch ($this->CurrentAction) {
			case "I": // Get a record to display
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("profilelist.php"); // No matching record, return to list
				}
				break;
			Case "U": // Update
				$this->SendEmail = TRUE; // Send email on update success
				if ($this->EditRow()) { // Update record based on key
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("UpdateSuccess")); // Update success
					$sReturnUrl = $this->getReturnUrl();
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Restore form values if update failed
				}
		}

		// Render the record
		$this->RowType = EW_ROWTYPE_EDIT; // Render as Edit
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm;

		// Get upload data
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->id->FldIsDetailKey)
			$this->id->setFormValue($objForm->GetValue("x_id"));
		if (!$this->firstname->FldIsDetailKey) {
			$this->firstname->setFormValue($objForm->GetValue("x_firstname"));
		}
		if (!$this->lastname->FldIsDetailKey) {
			$this->lastname->setFormValue($objForm->GetValue("x_lastname"));
		}
		if (!$this->location->FldIsDetailKey) {
			$this->location->setFormValue($objForm->GetValue("x_location"));
		}
		if (!$this->user->FldIsDetailKey) {
			$this->user->setFormValue($objForm->GetValue("x_user"));
		}
		if (!$this->pass->FldIsDetailKey) {
			$this->pass->setFormValue($objForm->GetValue("x_pass"));
		}
		if (!$this->_email->FldIsDetailKey) {
			$this->_email->setFormValue($objForm->GetValue("x__email"));
		}
		if (!$this->a_balance->FldIsDetailKey) {
			$this->a_balance->setFormValue($objForm->GetValue("x_a_balance"));
		}
		if (!$this->c_balance->FldIsDetailKey) {
			$this->c_balance->setFormValue($objForm->GetValue("x_c_balance"));
		}
		if (!$this->ac_no->FldIsDetailKey) {
			$this->ac_no->setFormValue($objForm->GetValue("x_ac_no"));
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadRow();
		$this->id->CurrentValue = $this->id->FormValue;
		$this->firstname->CurrentValue = $this->firstname->FormValue;
		$this->lastname->CurrentValue = $this->lastname->FormValue;
		$this->location->CurrentValue = $this->location->FormValue;
		$this->user->CurrentValue = $this->user->FormValue;
		$this->pass->CurrentValue = $this->pass->FormValue;
		$this->_email->CurrentValue = $this->_email->FormValue;
		$this->a_balance->CurrentValue = $this->a_balance->FormValue;
		$this->c_balance->CurrentValue = $this->c_balance->FormValue;
		$this->ac_no->CurrentValue = $this->ac_no->FormValue;
	}

	// Load row based on key values
	function LoadRow() {
		global $conn, $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		global $conn;
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->id->setDbValue($rs->fields('id'));
		$this->firstname->setDbValue($rs->fields('firstname'));
		$this->lastname->setDbValue($rs->fields('lastname'));
		$this->location->setDbValue($rs->fields('location'));
		$this->user->setDbValue($rs->fields('user'));
		$this->pass->setDbValue($rs->fields('pass'));
		$this->_email->setDbValue($rs->fields('email'));
		$this->a_balance->setDbValue($rs->fields('a_balance'));
		$this->c_balance->setDbValue($rs->fields('c_balance'));
		$this->ac_no->setDbValue($rs->fields('ac_no'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->id->DbValue = $row['id'];
		$this->firstname->DbValue = $row['firstname'];
		$this->lastname->DbValue = $row['lastname'];
		$this->location->DbValue = $row['location'];
		$this->user->DbValue = $row['user'];
		$this->pass->DbValue = $row['pass'];
		$this->_email->DbValue = $row['email'];
		$this->a_balance->DbValue = $row['a_balance'];
		$this->c_balance->DbValue = $row['c_balance'];
		$this->ac_no->DbValue = $row['ac_no'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $conn, $Security, $Language;
		global $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// id
		// firstname
		// lastname
		// location
		// user
		// pass
		// email
		// a_balance
		// c_balance
		// ac_no

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

			// id
			$this->id->ViewValue = $this->id->CurrentValue;
			$this->id->ViewCustomAttributes = "";

			// firstname
			$this->firstname->ViewValue = $this->firstname->CurrentValue;
			$this->firstname->ViewCustomAttributes = "";

			// lastname
			$this->lastname->ViewValue = $this->lastname->CurrentValue;
			$this->lastname->ViewCustomAttributes = "";

			// location
			$this->location->ViewValue = $this->location->CurrentValue;
			$this->location->ViewCustomAttributes = "";

			// user
			$this->user->ViewValue = $this->user->CurrentValue;
			$this->user->ViewCustomAttributes = "";

			// pass
			$this->pass->ViewValue = $this->pass->CurrentValue;
			$this->pass->ViewCustomAttributes = "";

			// email
			$this->_email->ViewValue = $this->_email->CurrentValue;
			$this->_email->ViewCustomAttributes = "";

			// a_balance
			$this->a_balance->ViewValue = $this->a_balance->CurrentValue;
			$this->a_balance->ViewCustomAttributes = "";

			// c_balance
			$this->c_balance->ViewValue = $this->c_balance->CurrentValue;
			$this->c_balance->ViewCustomAttributes = "";

			// ac_no
			$this->ac_no->ViewValue = $this->ac_no->CurrentValue;
			$this->ac_no->ViewCustomAttributes = "";

			// id
			$this->id->LinkCustomAttributes = "";
			$this->id->HrefValue = "";
			$this->id->TooltipValue = "";

			// firstname
			$this->firstname->LinkCustomAttributes = "";
			$this->firstname->HrefValue = "";
			$this->firstname->TooltipValue = "";

			// lastname
			$this->lastname->LinkCustomAttributes = "";
			$this->lastname->HrefValue = "";
			$this->lastname->TooltipValue = "";

			// location
			$this->location->LinkCustomAttributes = "";
			$this->location->HrefValue = "";
			$this->location->TooltipValue = "";

			// user
			$this->user->LinkCustomAttributes = "";
			$this->user->HrefValue = "";
			$this->user->TooltipValue = "";

			// pass
			$this->pass->LinkCustomAttributes = "";
			$this->pass->HrefValue = "";
			$this->pass->TooltipValue = "";

			// email
			$this->_email->LinkCustomAttributes = "";
			$this->_email->HrefValue = "";
			$this->_email->TooltipValue = "";

			// a_balance
			$this->a_balance->LinkCustomAttributes = "";
			$this->a_balance->HrefValue = "";
			$this->a_balance->TooltipValue = "";

			// c_balance
			$this->c_balance->LinkCustomAttributes = "";
			$this->c_balance->HrefValue = "";
			$this->c_balance->TooltipValue = "";

			// ac_no
			$this->ac_no->LinkCustomAttributes = "";
			$this->ac_no->HrefValue = "";
			$this->ac_no->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_EDIT) { // Edit row

			// id
			$this->id->EditCustomAttributes = "";
			$this->id->EditValue = $this->id->CurrentValue;
			$this->id->ViewCustomAttributes = "";

			// firstname
			$this->firstname->EditCustomAttributes = "";
			$this->firstname->EditValue = ew_HtmlEncode($this->firstname->CurrentValue);
			$this->firstname->PlaceHolder = ew_HtmlEncode(ew_RemoveHtml($this->firstname->FldCaption()));

			// lastname
			$this->lastname->EditCustomAttributes = "";
			$this->lastname->EditValue = ew_HtmlEncode($this->lastname->CurrentValue);
			$this->lastname->PlaceHolder = ew_HtmlEncode(ew_RemoveHtml($this->lastname->FldCaption()));

			// location
			$this->location->EditCustomAttributes = "";
			$this->location->EditValue = ew_HtmlEncode($this->location->CurrentValue);
			$this->location->PlaceHolder = ew_HtmlEncode(ew_RemoveHtml($this->location->FldCaption()));

			// user
			$this->user->EditCustomAttributes = "";
			$this->user->EditValue = ew_HtmlEncode($this->user->CurrentValue);
			$this->user->PlaceHolder = ew_HtmlEncode(ew_RemoveHtml($this->user->FldCaption()));

			// pass
			$this->pass->EditCustomAttributes = "";
			$this->pass->EditValue = ew_HtmlEncode($this->pass->CurrentValue);
			$this->pass->PlaceHolder = ew_HtmlEncode(ew_RemoveHtml($this->pass->FldCaption()));

			// email
			$this->_email->EditCustomAttributes = "";
			$this->_email->EditValue = ew_HtmlEncode($this->_email->CurrentValue);
			$this->_email->PlaceHolder = ew_HtmlEncode(ew_RemoveHtml($this->_email->FldCaption()));

			// a_balance
			$this->a_balance->EditCustomAttributes = "";
			$this->a_balance->EditValue = ew_HtmlEncode($this->a_balance->CurrentValue);
			$this->a_balance->PlaceHolder = ew_HtmlEncode(ew_RemoveHtml($this->a_balance->FldCaption()));

			// c_balance
			$this->c_balance->EditCustomAttributes = "";
			$this->c_balance->EditValue = ew_HtmlEncode($this->c_balance->CurrentValue);
			$this->c_balance->PlaceHolder = ew_HtmlEncode(ew_RemoveHtml($this->c_balance->FldCaption()));

			// ac_no
			$this->ac_no->EditCustomAttributes = "";
			$this->ac_no->EditValue = ew_HtmlEncode($this->ac_no->CurrentValue);
			$this->ac_no->PlaceHolder = ew_HtmlEncode(ew_RemoveHtml($this->ac_no->FldCaption()));

			// Edit refer script
			// id

			$this->id->HrefValue = "";

			// firstname
			$this->firstname->HrefValue = "";

			// lastname
			$this->lastname->HrefValue = "";

			// location
			$this->location->HrefValue = "";

			// user
			$this->user->HrefValue = "";

			// pass
			$this->pass->HrefValue = "";

			// email
			$this->_email->HrefValue = "";

			// a_balance
			$this->a_balance->HrefValue = "";

			// c_balance
			$this->c_balance->HrefValue = "";

			// ac_no
			$this->ac_no->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->firstname->FldIsDetailKey && !is_null($this->firstname->FormValue) && $this->firstname->FormValue == "") {
			ew_AddMessage($gsFormError, $Language->Phrase("EnterRequiredField") . " - " . $this->firstname->FldCaption());
		}
		if (!$this->lastname->FldIsDetailKey && !is_null($this->lastname->FormValue) && $this->lastname->FormValue == "") {
			ew_AddMessage($gsFormError, $Language->Phrase("EnterRequiredField") . " - " . $this->lastname->FldCaption());
		}
		if (!$this->location->FldIsDetailKey && !is_null($this->location->FormValue) && $this->location->FormValue == "") {
			ew_AddMessage($gsFormError, $Language->Phrase("EnterRequiredField") . " - " . $this->location->FldCaption());
		}
		if (!$this->user->FldIsDetailKey && !is_null($this->user->FormValue) && $this->user->FormValue == "") {
			ew_AddMessage($gsFormError, $Language->Phrase("EnterRequiredField") . " - " . $this->user->FldCaption());
		}
		if (!$this->pass->FldIsDetailKey && !is_null($this->pass->FormValue) && $this->pass->FormValue == "") {
			ew_AddMessage($gsFormError, $Language->Phrase("EnterRequiredField") . " - " . $this->pass->FldCaption());
		}
		if (!$this->_email->FldIsDetailKey && !is_null($this->_email->FormValue) && $this->_email->FormValue == "") {
			ew_AddMessage($gsFormError, $Language->Phrase("EnterRequiredField") . " - " . $this->_email->FldCaption());
		}
		if (!$this->a_balance->FldIsDetailKey && !is_null($this->a_balance->FormValue) && $this->a_balance->FormValue == "") {
			ew_AddMessage($gsFormError, $Language->Phrase("EnterRequiredField") . " - " . $this->a_balance->FldCaption());
		}
		if (!ew_CheckInteger($this->a_balance->FormValue)) {
			ew_AddMessage($gsFormError, $this->a_balance->FldErrMsg());
		}
		if (!$this->c_balance->FldIsDetailKey && !is_null($this->c_balance->FormValue) && $this->c_balance->FormValue == "") {
			ew_AddMessage($gsFormError, $Language->Phrase("EnterRequiredField") . " - " . $this->c_balance->FldCaption());
		}
		if (!ew_CheckInteger($this->c_balance->FormValue)) {
			ew_AddMessage($gsFormError, $this->c_balance->FldErrMsg());
		}
		if (!$this->ac_no->FldIsDetailKey && !is_null($this->ac_no->FormValue) && $this->ac_no->FormValue == "") {
			ew_AddMessage($gsFormError, $Language->Phrase("EnterRequiredField") . " - " . $this->ac_no->FldCaption());
		}
		if (!ew_CheckInteger($this->ac_no->FormValue)) {
			ew_AddMessage($gsFormError, $this->ac_no->FldErrMsg());
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Update record based on key values
	function EditRow() {
		global $conn, $Security, $Language;
		$sFilter = $this->KeyFilter();
			if ($this->ac_no->CurrentValue <> "") { // Check field with unique index
			$sFilterChk = "(`ac_no` = " . ew_AdjustSql($this->ac_no->CurrentValue) . ")";
			$sFilterChk .= " AND NOT (" . $sFilter . ")";
			$this->CurrentFilter = $sFilterChk;
			$sSqlChk = $this->SQL();
			$conn->raiseErrorFn = 'ew_ErrorFn';
			$rsChk = $conn->Execute($sSqlChk);
			$conn->raiseErrorFn = '';
			if ($rsChk === FALSE) {
				return FALSE;
			} elseif (!$rsChk->EOF) {
				$sIdxErrMsg = str_replace("%f", $this->ac_no->FldCaption(), $Language->Phrase("DupIndex"));
				$sIdxErrMsg = str_replace("%v", $this->ac_no->CurrentValue, $sIdxErrMsg);
				$this->setFailureMessage($sIdxErrMsg);
				$rsChk->Close();
				return FALSE;
			}
			$rsChk->Close();
		}
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = 'ew_ErrorFn';
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE)
			return FALSE;
		if ($rs->EOF) {
			$EditRow = FALSE; // Update Failed
		} else {

			// Save old values
			$rsold = &$rs->fields;
			$this->LoadDbValues($rsold);
			$rsnew = array();

			// firstname
			$this->firstname->SetDbValueDef($rsnew, $this->firstname->CurrentValue, "", $this->firstname->ReadOnly);

			// lastname
			$this->lastname->SetDbValueDef($rsnew, $this->lastname->CurrentValue, "", $this->lastname->ReadOnly);

			// location
			$this->location->SetDbValueDef($rsnew, $this->location->CurrentValue, "", $this->location->ReadOnly);

			// user
			$this->user->SetDbValueDef($rsnew, $this->user->CurrentValue, "", $this->user->ReadOnly);

			// pass
			$this->pass->SetDbValueDef($rsnew, $this->pass->CurrentValue, "", $this->pass->ReadOnly);

			// email
			$this->_email->SetDbValueDef($rsnew, $this->_email->CurrentValue, "", $this->_email->ReadOnly);

			// a_balance
			$this->a_balance->SetDbValueDef($rsnew, $this->a_balance->CurrentValue, 0, $this->a_balance->ReadOnly);

			// c_balance
			$this->c_balance->SetDbValueDef($rsnew, $this->c_balance->CurrentValue, 0, $this->c_balance->ReadOnly);

			// ac_no
			$this->ac_no->SetDbValueDef($rsnew, $this->ac_no->CurrentValue, 0, $this->ac_no->ReadOnly);

			// Call Row Updating event
			$bUpdateRow = $this->Row_Updating($rsold, $rsnew);
			if ($bUpdateRow) {
				$conn->raiseErrorFn = 'ew_ErrorFn';
				if (count($rsnew) > 0)
					$EditRow = $this->Update($rsnew, "", $rsold);
				else
					$EditRow = TRUE; // No field to update
				$conn->raiseErrorFn = '';
				if ($EditRow) {
				}
			} else {
				if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

					// Use the message, do nothing
				} elseif ($this->CancelMessage <> "") {
					$this->setFailureMessage($this->CancelMessage);
					$this->CancelMessage = "";
				} else {
					$this->setFailureMessage($Language->Phrase("UpdateCancelled"));
				}
				$EditRow = FALSE;
			}
		}

		// Call Row_Updated event
		if ($EditRow)
			$this->Row_Updated($rsold, $rsnew);
		$rs->Close();
		return $EditRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$PageCaption = $this->TableCaption();
		$Breadcrumb->Add("list", "<span id=\"ewPageCaption\">" . $PageCaption . "</span>", "profilelist.php", $this->TableVar);
		$PageCaption = $Language->Phrase("edit");
		$Breadcrumb->Add("edit", "<span id=\"ewPageCaption\">" . $PageCaption . "</span>", ew_CurrentUrl(), $this->TableVar);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($profile_edit)) $profile_edit = new cprofile_edit();

// Page init
$profile_edit->Page_Init();

// Page main
$profile_edit->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$profile_edit->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Page object
var profile_edit = new ew_Page("profile_edit");
profile_edit.PageID = "edit"; // Page ID
var EW_PAGE_ID = profile_edit.PageID; // For backward compatibility

// Form object
var fprofileedit = new ew_Form("fprofileedit");

// Validate form
fprofileedit.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	this.PostAutoSuggest();
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_firstname");
			if (elm && !ew_HasValue(elm))
				return this.OnError(elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($profile->firstname->FldCaption()) ?>");
			elm = this.GetElements("x" + infix + "_lastname");
			if (elm && !ew_HasValue(elm))
				return this.OnError(elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($profile->lastname->FldCaption()) ?>");
			elm = this.GetElements("x" + infix + "_location");
			if (elm && !ew_HasValue(elm))
				return this.OnError(elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($profile->location->FldCaption()) ?>");
			elm = this.GetElements("x" + infix + "_user");
			if (elm && !ew_HasValue(elm))
				return this.OnError(elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($profile->user->FldCaption()) ?>");
			elm = this.GetElements("x" + infix + "_pass");
			if (elm && !ew_HasValue(elm))
				return this.OnError(elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($profile->pass->FldCaption()) ?>");
			elm = this.GetElements("x" + infix + "__email");
			if (elm && !ew_HasValue(elm))
				return this.OnError(elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($profile->_email->FldCaption()) ?>");
			elm = this.GetElements("x" + infix + "_a_balance");
			if (elm && !ew_HasValue(elm))
				return this.OnError(elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($profile->a_balance->FldCaption()) ?>");
			elm = this.GetElements("x" + infix + "_a_balance");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($profile->a_balance->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_c_balance");
			if (elm && !ew_HasValue(elm))
				return this.OnError(elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($profile->c_balance->FldCaption()) ?>");
			elm = this.GetElements("x" + infix + "_c_balance");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($profile->c_balance->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_ac_no");
			if (elm && !ew_HasValue(elm))
				return this.OnError(elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($profile->ac_no->FldCaption()) ?>");
			elm = this.GetElements("x" + infix + "_ac_no");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($profile->ac_no->FldErrMsg()) ?>");

			// Set up row object
			ew_ElementsToRow(fobj);

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
fprofileedit.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fprofileedit.ValidateRequired = true;
<?php } else { ?>
fprofileedit.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
// Form object for search

</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php $Breadcrumb->Render(); ?>
<?php $profile_edit->ShowPageHeader(); ?>
<?php
$profile_edit->ShowMessage();
?>
<form name="fprofileedit" id="fprofileedit" class="ewForm form-horizontal" action="<?php echo ew_CurrentPage() ?>" method="post">
<input type="hidden" name="t" value="profile">
<input type="hidden" name="a_edit" id="a_edit" value="U">
<table cellspacing="0" class="ewGrid"><tr><td>
<table id="tbl_profileedit" class="table table-bordered table-striped">
<?php if ($profile->id->Visible) { // id ?>
	<tr id="r_id"<?php echo $profile->RowAttributes() ?>>
		<td><span id="elh_profile_id"><?php echo $profile->id->FldCaption() ?></span></td>
		<td<?php echo $profile->id->CellAttributes() ?>><span id="el_profile_id" class="control-group">
<span<?php echo $profile->id->ViewAttributes() ?>>
<?php echo $profile->id->EditValue ?></span>
<input type="hidden" data-field="x_id" name="x_id" id="x_id" value="<?php echo ew_HtmlEncode($profile->id->CurrentValue) ?>">
</span><?php echo $profile->id->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($profile->firstname->Visible) { // firstname ?>
	<tr id="r_firstname"<?php echo $profile->RowAttributes() ?>>
		<td><span id="elh_profile_firstname"><?php echo $profile->firstname->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></span></td>
		<td<?php echo $profile->firstname->CellAttributes() ?>><span id="el_profile_firstname" class="control-group">
<input type="text" data-field="x_firstname" name="x_firstname" id="x_firstname" size="30" maxlength="25" placeholder="<?php echo $profile->firstname->PlaceHolder ?>" value="<?php echo $profile->firstname->EditValue ?>"<?php echo $profile->firstname->EditAttributes() ?>>
</span><?php echo $profile->firstname->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($profile->lastname->Visible) { // lastname ?>
	<tr id="r_lastname"<?php echo $profile->RowAttributes() ?>>
		<td><span id="elh_profile_lastname"><?php echo $profile->lastname->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></span></td>
		<td<?php echo $profile->lastname->CellAttributes() ?>><span id="el_profile_lastname" class="control-group">
<input type="text" data-field="x_lastname" name="x_lastname" id="x_lastname" size="30" maxlength="25" placeholder="<?php echo $profile->lastname->PlaceHolder ?>" value="<?php echo $profile->lastname->EditValue ?>"<?php echo $profile->lastname->EditAttributes() ?>>
</span><?php echo $profile->lastname->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($profile->location->Visible) { // location ?>
	<tr id="r_location"<?php echo $profile->RowAttributes() ?>>
		<td><span id="elh_profile_location"><?php echo $profile->location->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></span></td>
		<td<?php echo $profile->location->CellAttributes() ?>><span id="el_profile_location" class="control-group">
<input type="text" data-field="x_location" name="x_location" id="x_location" size="30" maxlength="25" placeholder="<?php echo $profile->location->PlaceHolder ?>" value="<?php echo $profile->location->EditValue ?>"<?php echo $profile->location->EditAttributes() ?>>
</span><?php echo $profile->location->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($profile->user->Visible) { // user ?>
	<tr id="r_user"<?php echo $profile->RowAttributes() ?>>
		<td><span id="elh_profile_user"><?php echo $profile->user->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></span></td>
		<td<?php echo $profile->user->CellAttributes() ?>><span id="el_profile_user" class="control-group">
<input type="text" data-field="x_user" name="x_user" id="x_user" size="30" maxlength="20" placeholder="<?php echo $profile->user->PlaceHolder ?>" value="<?php echo $profile->user->EditValue ?>"<?php echo $profile->user->EditAttributes() ?>>
</span><?php echo $profile->user->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($profile->pass->Visible) { // pass ?>
	<tr id="r_pass"<?php echo $profile->RowAttributes() ?>>
		<td><span id="elh_profile_pass"><?php echo $profile->pass->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></span></td>
		<td<?php echo $profile->pass->CellAttributes() ?>><span id="el_profile_pass" class="control-group">
<input type="text" data-field="x_pass" name="x_pass" id="x_pass" size="30" maxlength="25" placeholder="<?php echo $profile->pass->PlaceHolder ?>" value="<?php echo $profile->pass->EditValue ?>"<?php echo $profile->pass->EditAttributes() ?>>
</span><?php echo $profile->pass->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($profile->_email->Visible) { // email ?>
	<tr id="r__email"<?php echo $profile->RowAttributes() ?>>
		<td><span id="elh_profile__email"><?php echo $profile->_email->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></span></td>
		<td<?php echo $profile->_email->CellAttributes() ?>><span id="el_profile__email" class="control-group">
<input type="text" data-field="x__email" name="x__email" id="x__email" size="30" maxlength="30" placeholder="<?php echo $profile->_email->PlaceHolder ?>" value="<?php echo $profile->_email->EditValue ?>"<?php echo $profile->_email->EditAttributes() ?>>
</span><?php echo $profile->_email->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($profile->a_balance->Visible) { // a_balance ?>
	<tr id="r_a_balance"<?php echo $profile->RowAttributes() ?>>
		<td><span id="elh_profile_a_balance"><?php echo $profile->a_balance->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></span></td>
		<td<?php echo $profile->a_balance->CellAttributes() ?>><span id="el_profile_a_balance" class="control-group">
<input type="text" data-field="x_a_balance" name="x_a_balance" id="x_a_balance" size="30" placeholder="<?php echo $profile->a_balance->PlaceHolder ?>" value="<?php echo $profile->a_balance->EditValue ?>"<?php echo $profile->a_balance->EditAttributes() ?>>
</span><?php echo $profile->a_balance->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($profile->c_balance->Visible) { // c_balance ?>
	<tr id="r_c_balance"<?php echo $profile->RowAttributes() ?>>
		<td><span id="elh_profile_c_balance"><?php echo $profile->c_balance->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></span></td>
		<td<?php echo $profile->c_balance->CellAttributes() ?>><span id="el_profile_c_balance" class="control-group">
<input type="text" data-field="x_c_balance" name="x_c_balance" id="x_c_balance" size="30" placeholder="<?php echo $profile->c_balance->PlaceHolder ?>" value="<?php echo $profile->c_balance->EditValue ?>"<?php echo $profile->c_balance->EditAttributes() ?>>
</span><?php echo $profile->c_balance->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($profile->ac_no->Visible) { // ac_no ?>
	<tr id="r_ac_no"<?php echo $profile->RowAttributes() ?>>
		<td><span id="elh_profile_ac_no"><?php echo $profile->ac_no->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></span></td>
		<td<?php echo $profile->ac_no->CellAttributes() ?>><span id="el_profile_ac_no" class="control-group">
<input type="text" data-field="x_ac_no" name="x_ac_no" id="x_ac_no" size="30" placeholder="<?php echo $profile->ac_no->PlaceHolder ?>" value="<?php echo $profile->ac_no->EditValue ?>"<?php echo $profile->ac_no->EditAttributes() ?>>
</span><?php echo $profile->ac_no->CustomMsg ?></td>
	</tr>
<?php } ?>
</table>
</td></tr></table>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("EditBtn") ?></button>
</form>
<script type="text/javascript">
fprofileedit.Init();
<?php if (EW_MOBILE_REFLOW && ew_IsMobile()) { ?>
ew_Reflow();
<?php } ?>
</script>
<?php
$profile_edit->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$profile_edit->Page_Terminate();
?>
