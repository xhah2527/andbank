<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg10.php" ?>
<?php include_once "ewmysql10.php" ?>
<?php include_once "phpfn10.php" ?>
<?php include_once "transfersinfo.php" ?>
<?php include_once "admin_usersinfo.php" ?>
<?php include_once "userfn10.php" ?>
<?php

//
// Page class
//

$transfers_add = NULL; // Initialize page object first

class ctransfers_add extends ctransfers {

	// Page ID
	var $PageID = 'add';

	// Project ID
	var $ProjectID = "{830E57EC-9344-4D0E-802B-B652CCAA29BD}";

	// Table name
	var $TableName = 'transfers';

	// Page object name
	var $PageObjName = 'transfers_add';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-error ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<table class=\"ewStdTable\"><tr><td><div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div></td></tr></table>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language, $UserAgent;

		// User agent
		$UserAgent = ew_UserAgent();
		$GLOBALS["Page"] = &$this;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (transfers)
		if (!isset($GLOBALS["transfers"])) {
			$GLOBALS["transfers"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["transfers"];
		}

		// Table object (admin_users)
		if (!isset($GLOBALS['admin_users'])) $GLOBALS['admin_users'] = new cadmin_users();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'add', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'transfers', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect();
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if (!$Security->IsLoggedIn()) {
			$Security->SaveLastUrl();
			$this->Page_Terminate("login.php");
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up curent action

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $conn;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();
		$this->Page_Redirecting($url);

		 // Close connection
		$conn->Close();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $Priv = 0;
	var $OldRecordset;
	var $CopyRecord;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;

		// Process form if post back
		if (@$_POST["a_add"] <> "") {
			$this->CurrentAction = $_POST["a_add"]; // Get form action
			$this->CopyRecord = $this->LoadOldRecord(); // Load old recordset
			$this->LoadFormValues(); // Load form values
		} else { // Not post back

			// Load key values from QueryString
			$this->CopyRecord = TRUE;
			if (@$_GET["id"] != "") {
				$this->id->setQueryStringValue($_GET["id"]);
				$this->setKey("id", $this->id->CurrentValue); // Set up key
			} else {
				$this->setKey("id", ""); // Clear key
				$this->CopyRecord = FALSE;
			}
			if ($this->CopyRecord) {
				$this->CurrentAction = "C"; // Copy record
			} else {
				$this->CurrentAction = "I"; // Display blank record
				$this->LoadDefaultValues(); // Load default values
			}
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Validate form if post back
		if (@$_POST["a_add"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = "I"; // Form error, reset action
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues(); // Restore form values
				$this->setFailureMessage($gsFormError);
			}
		}

		// Perform action based on action code
		switch ($this->CurrentAction) {
			case "I": // Blank record, no action required
				break;
			case "C": // Copy an existing record
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("transferslist.php"); // No matching record, return to list
				}
				break;
			case "A": // Add new record
				$this->SendEmail = TRUE; // Send email on add success
				if ($this->AddRow($this->OldRecordset)) { // Add successful
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("AddSuccess")); // Set up success message
					$sReturnUrl = $this->getReturnUrl();
					if (ew_GetPageName($sReturnUrl) == "transfersview.php")
						$sReturnUrl = $this->GetViewUrl(); // View paging, return to view page with keyurl directly
					$this->Page_Terminate($sReturnUrl); // Clean up and return
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Add failed, restore form values
				}
		}

		// Render row based on row type
		$this->RowType = EW_ROWTYPE_ADD;  // Render add type

		// Render row
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm;

		// Get upload data
	}

	// Load default values
	function LoadDefaultValues() {
		$this->sender->CurrentValue = NULL;
		$this->sender->OldValue = $this->sender->CurrentValue;
		$this->account_no->CurrentValue = NULL;
		$this->account_no->OldValue = $this->account_no->CurrentValue;
		$this->transaction_amount->CurrentValue = NULL;
		$this->transaction_amount->OldValue = $this->transaction_amount->CurrentValue;
		$this->transaction_id->CurrentValue = NULL;
		$this->transaction_id->OldValue = $this->transaction_id->CurrentValue;
		$this->transaction_date->CurrentValue = NULL;
		$this->transaction_date->OldValue = $this->transaction_date->CurrentValue;
		$this->transaction_status->CurrentValue = NULL;
		$this->transaction_status->OldValue = $this->transaction_status->CurrentValue;
		$this->trnx_desc->CurrentValue = NULL;
		$this->trnx_desc->OldValue = $this->trnx_desc->CurrentValue;
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->sender->FldIsDetailKey) {
			$this->sender->setFormValue($objForm->GetValue("x_sender"));
		}
		if (!$this->account_no->FldIsDetailKey) {
			$this->account_no->setFormValue($objForm->GetValue("x_account_no"));
		}
		if (!$this->transaction_amount->FldIsDetailKey) {
			$this->transaction_amount->setFormValue($objForm->GetValue("x_transaction_amount"));
		}
		if (!$this->transaction_id->FldIsDetailKey) {
			$this->transaction_id->setFormValue($objForm->GetValue("x_transaction_id"));
		}
		if (!$this->transaction_date->FldIsDetailKey) {
			$this->transaction_date->setFormValue($objForm->GetValue("x_transaction_date"));
			$this->transaction_date->CurrentValue = ew_UnFormatDateTime($this->transaction_date->CurrentValue, 6);
		}
		if (!$this->transaction_status->FldIsDetailKey) {
			$this->transaction_status->setFormValue($objForm->GetValue("x_transaction_status"));
		}
		if (!$this->trnx_desc->FldIsDetailKey) {
			$this->trnx_desc->setFormValue($objForm->GetValue("x_trnx_desc"));
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadOldRecord();
		$this->sender->CurrentValue = $this->sender->FormValue;
		$this->account_no->CurrentValue = $this->account_no->FormValue;
		$this->transaction_amount->CurrentValue = $this->transaction_amount->FormValue;
		$this->transaction_id->CurrentValue = $this->transaction_id->FormValue;
		$this->transaction_date->CurrentValue = $this->transaction_date->FormValue;
		$this->transaction_date->CurrentValue = ew_UnFormatDateTime($this->transaction_date->CurrentValue, 6);
		$this->transaction_status->CurrentValue = $this->transaction_status->FormValue;
		$this->trnx_desc->CurrentValue = $this->trnx_desc->FormValue;
	}

	// Load row based on key values
	function LoadRow() {
		global $conn, $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		global $conn;
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->id->setDbValue($rs->fields('id'));
		$this->sender->setDbValue($rs->fields('sender'));
		$this->account_no->setDbValue($rs->fields('account_no'));
		$this->transaction_amount->setDbValue($rs->fields('transaction_amount'));
		$this->transaction_id->setDbValue($rs->fields('transaction_id'));
		$this->transaction_date->setDbValue($rs->fields('transaction_date'));
		$this->transaction_status->setDbValue($rs->fields('transaction_status'));
		$this->trnx_desc->setDbValue($rs->fields('trnx_desc'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->id->DbValue = $row['id'];
		$this->sender->DbValue = $row['sender'];
		$this->account_no->DbValue = $row['account_no'];
		$this->transaction_amount->DbValue = $row['transaction_amount'];
		$this->transaction_id->DbValue = $row['transaction_id'];
		$this->transaction_date->DbValue = $row['transaction_date'];
		$this->transaction_status->DbValue = $row['transaction_status'];
		$this->trnx_desc->DbValue = $row['trnx_desc'];
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("id")) <> "")
			$this->id->CurrentValue = $this->getKey("id"); // id
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$this->OldRecordset = ew_LoadRecordset($sSql);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $conn, $Security, $Language;
		global $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// id
		// sender
		// account_no
		// transaction_amount
		// transaction_id
		// transaction_date
		// transaction_status
		// trnx_desc

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

			// id
			$this->id->ViewValue = $this->id->CurrentValue;
			$this->id->ViewCustomAttributes = "";

			// sender
			$this->sender->ViewValue = $this->sender->CurrentValue;
			$this->sender->ViewCustomAttributes = "";

			// account_no
			if (strval($this->account_no->CurrentValue) <> "") {
				$sFilterWrk = "`ac_no`" . ew_SearchString("=", $this->account_no->CurrentValue, EW_DATATYPE_NUMBER);
			$sSqlWrk = "SELECT `ac_no`, `email` AS `DispFld`, `user` AS `Disp2Fld`, `ac_no` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `profile`";
			$sWhereWrk = "";
			if ($sFilterWrk <> "") {
				ew_AddFilter($sWhereWrk, $sFilterWrk);
			}

			// Call Lookup selecting
			$this->Lookup_Selecting($this->account_no, $sWhereWrk);
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
				$rswrk = $conn->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$this->account_no->ViewValue = $rswrk->fields('DispFld');
					$this->account_no->ViewValue .= ew_ValueSeparator(1,$this->account_no) . $rswrk->fields('Disp2Fld');
					$this->account_no->ViewValue .= ew_ValueSeparator(2,$this->account_no) . $rswrk->fields('Disp3Fld');
					$rswrk->Close();
				} else {
					$this->account_no->ViewValue = $this->account_no->CurrentValue;
				}
			} else {
				$this->account_no->ViewValue = NULL;
			}
			$this->account_no->ViewCustomAttributes = "";

			// transaction_amount
			$this->transaction_amount->ViewValue = $this->transaction_amount->CurrentValue;
			$this->transaction_amount->ViewCustomAttributes = "";

			// transaction_id
			$this->transaction_id->ViewValue = $this->transaction_id->CurrentValue;
			$this->transaction_id->ViewCustomAttributes = "";

			// transaction_date
			$this->transaction_date->ViewValue = $this->transaction_date->CurrentValue;
			$this->transaction_date->ViewValue = ew_FormatDateTime($this->transaction_date->ViewValue, 6);
			$this->transaction_date->ViewCustomAttributes = "";

			// transaction_status
			$this->transaction_status->ViewValue = $this->transaction_status->CurrentValue;
			$this->transaction_status->ViewCustomAttributes = "";

			// trnx_desc
			$this->trnx_desc->ViewValue = $this->trnx_desc->CurrentValue;
			$this->trnx_desc->ViewCustomAttributes = "";

			// sender
			$this->sender->LinkCustomAttributes = "";
			$this->sender->HrefValue = "";
			$this->sender->TooltipValue = "";

			// account_no
			$this->account_no->LinkCustomAttributes = "";
			$this->account_no->HrefValue = "";
			$this->account_no->TooltipValue = "";

			// transaction_amount
			$this->transaction_amount->LinkCustomAttributes = "";
			$this->transaction_amount->HrefValue = "";
			$this->transaction_amount->TooltipValue = "";

			// transaction_id
			$this->transaction_id->LinkCustomAttributes = "";
			$this->transaction_id->HrefValue = "";
			$this->transaction_id->TooltipValue = "";

			// transaction_date
			$this->transaction_date->LinkCustomAttributes = "";
			$this->transaction_date->HrefValue = "";
			$this->transaction_date->TooltipValue = "";

			// transaction_status
			$this->transaction_status->LinkCustomAttributes = "";
			$this->transaction_status->HrefValue = "";
			$this->transaction_status->TooltipValue = "";

			// trnx_desc
			$this->trnx_desc->LinkCustomAttributes = "";
			$this->trnx_desc->HrefValue = "";
			$this->trnx_desc->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_ADD) { // Add row

			// sender
			$this->sender->EditCustomAttributes = "";
			$this->sender->EditValue = ew_HtmlEncode($this->sender->CurrentValue);
			$this->sender->PlaceHolder = ew_HtmlEncode(ew_RemoveHtml($this->sender->FldCaption()));

			// account_no
			$this->account_no->EditCustomAttributes = "";
			if (trim(strval($this->account_no->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`ac_no`" . ew_SearchString("=", $this->account_no->CurrentValue, EW_DATATYPE_NUMBER);
			}
			$sSqlWrk = "SELECT `ac_no`, `email` AS `DispFld`, `user` AS `Disp2Fld`, `ac_no` AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `profile`";
			$sWhereWrk = "";
			if ($sFilterWrk <> "") {
				ew_AddFilter($sWhereWrk, $sFilterWrk);
			}

			// Call Lookup selecting
			$this->Lookup_Selecting($this->account_no, $sWhereWrk);
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = $conn->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->account_no->EditValue = $arwrk;

			// transaction_amount
			$this->transaction_amount->EditCustomAttributes = "";
			$this->transaction_amount->EditValue = ew_HtmlEncode($this->transaction_amount->CurrentValue);
			$this->transaction_amount->PlaceHolder = ew_HtmlEncode(ew_RemoveHtml($this->transaction_amount->FldCaption()));

			// transaction_id
			$this->transaction_id->EditCustomAttributes = "";
			$this->transaction_id->EditValue = ew_HtmlEncode($this->transaction_id->CurrentValue);
			$this->transaction_id->PlaceHolder = ew_HtmlEncode(ew_RemoveHtml($this->transaction_id->FldCaption()));

			// transaction_date
			$this->transaction_date->EditCustomAttributes = "";
			$this->transaction_date->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->transaction_date->CurrentValue, 6));
			$this->transaction_date->PlaceHolder = ew_HtmlEncode(ew_RemoveHtml($this->transaction_date->FldCaption()));

			// transaction_status
			$this->transaction_status->EditCustomAttributes = "";
			$this->transaction_status->EditValue = ew_HtmlEncode($this->transaction_status->CurrentValue);
			$this->transaction_status->PlaceHolder = ew_HtmlEncode(ew_RemoveHtml($this->transaction_status->FldCaption()));

			// trnx_desc
			$this->trnx_desc->EditCustomAttributes = "";
			$this->trnx_desc->EditValue = $this->trnx_desc->CurrentValue;
			$this->trnx_desc->PlaceHolder = ew_HtmlEncode(ew_RemoveHtml($this->trnx_desc->FldCaption()));

			// Edit refer script
			// sender

			$this->sender->HrefValue = "";

			// account_no
			$this->account_no->HrefValue = "";

			// transaction_amount
			$this->transaction_amount->HrefValue = "";

			// transaction_id
			$this->transaction_id->HrefValue = "";

			// transaction_date
			$this->transaction_date->HrefValue = "";

			// transaction_status
			$this->transaction_status->HrefValue = "";

			// trnx_desc
			$this->trnx_desc->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->sender->FldIsDetailKey && !is_null($this->sender->FormValue) && $this->sender->FormValue == "") {
			ew_AddMessage($gsFormError, $Language->Phrase("EnterRequiredField") . " - " . $this->sender->FldCaption());
		}
		if (!$this->account_no->FldIsDetailKey && !is_null($this->account_no->FormValue) && $this->account_no->FormValue == "") {
			ew_AddMessage($gsFormError, $Language->Phrase("EnterRequiredField") . " - " . $this->account_no->FldCaption());
		}
		if (!$this->transaction_amount->FldIsDetailKey && !is_null($this->transaction_amount->FormValue) && $this->transaction_amount->FormValue == "") {
			ew_AddMessage($gsFormError, $Language->Phrase("EnterRequiredField") . " - " . $this->transaction_amount->FldCaption());
		}
		if (!ew_CheckInteger($this->transaction_amount->FormValue)) {
			ew_AddMessage($gsFormError, $this->transaction_amount->FldErrMsg());
		}
		if (!$this->transaction_id->FldIsDetailKey && !is_null($this->transaction_id->FormValue) && $this->transaction_id->FormValue == "") {
			ew_AddMessage($gsFormError, $Language->Phrase("EnterRequiredField") . " - " . $this->transaction_id->FldCaption());
		}
		if (!$this->transaction_date->FldIsDetailKey && !is_null($this->transaction_date->FormValue) && $this->transaction_date->FormValue == "") {
			ew_AddMessage($gsFormError, $Language->Phrase("EnterRequiredField") . " - " . $this->transaction_date->FldCaption());
		}
		if (!ew_CheckUSDate($this->transaction_date->FormValue)) {
			ew_AddMessage($gsFormError, $this->transaction_date->FldErrMsg());
		}
		if (!$this->transaction_status->FldIsDetailKey && !is_null($this->transaction_status->FormValue) && $this->transaction_status->FormValue == "") {
			ew_AddMessage($gsFormError, $Language->Phrase("EnterRequiredField") . " - " . $this->transaction_status->FldCaption());
		}
		if (!$this->trnx_desc->FldIsDetailKey && !is_null($this->trnx_desc->FormValue) && $this->trnx_desc->FormValue == "") {
			ew_AddMessage($gsFormError, $Language->Phrase("EnterRequiredField") . " - " . $this->trnx_desc->FldCaption());
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Add record
	function AddRow($rsold = NULL) {
		global $conn, $Language, $Security;

		// Load db values from rsold
		if ($rsold) {
			$this->LoadDbValues($rsold);
		}
		$rsnew = array();

		// sender
		$this->sender->SetDbValueDef($rsnew, $this->sender->CurrentValue, "", FALSE);

		// account_no
		$this->account_no->SetDbValueDef($rsnew, $this->account_no->CurrentValue, 0, FALSE);

		// transaction_amount
		$this->transaction_amount->SetDbValueDef($rsnew, $this->transaction_amount->CurrentValue, 0, FALSE);

		// transaction_id
		$this->transaction_id->SetDbValueDef($rsnew, $this->transaction_id->CurrentValue, "", FALSE);

		// transaction_date
		$this->transaction_date->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->transaction_date->CurrentValue, 6), ew_CurrentDate(), FALSE);

		// transaction_status
		$this->transaction_status->SetDbValueDef($rsnew, $this->transaction_status->CurrentValue, "", FALSE);

		// trnx_desc
		$this->trnx_desc->SetDbValueDef($rsnew, $this->trnx_desc->CurrentValue, "", FALSE);

		// Call Row Inserting event
		$rs = ($rsold == NULL) ? NULL : $rsold->fields;
		$bInsertRow = $this->Row_Inserting($rs, $rsnew);
		if ($bInsertRow) {
			$conn->raiseErrorFn = 'ew_ErrorFn';
			$AddRow = $this->Insert($rsnew);
			$conn->raiseErrorFn = '';
			if ($AddRow) {
			}
		} else {
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("InsertCancelled"));
			}
			$AddRow = FALSE;
		}

		// Get insert id if necessary
		if ($AddRow) {
			$this->id->setDbValue($conn->Insert_ID());
			$rsnew['id'] = $this->id->DbValue;
		}
		if ($AddRow) {

			// Call Row Inserted event
			$rs = ($rsold == NULL) ? NULL : $rsold->fields;
			$this->Row_Inserted($rs, $rsnew);
		}
		return $AddRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$PageCaption = $this->TableCaption();
		$Breadcrumb->Add("list", "<span id=\"ewPageCaption\">" . $PageCaption . "</span>", "transferslist.php", $this->TableVar);
		$PageCaption = ($this->CurrentAction == "C") ? $Language->Phrase("Copy") : $Language->Phrase("Add");
		$Breadcrumb->Add("add", "<span id=\"ewPageCaption\">" . $PageCaption . "</span>", ew_CurrentUrl(), $this->TableVar);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($transfers_add)) $transfers_add = new ctransfers_add();

// Page init
$transfers_add->Page_Init();

// Page main
$transfers_add->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$transfers_add->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Page object
var transfers_add = new ew_Page("transfers_add");
transfers_add.PageID = "add"; // Page ID
var EW_PAGE_ID = transfers_add.PageID; // For backward compatibility

// Form object
var ftransfersadd = new ew_Form("ftransfersadd");

// Validate form
ftransfersadd.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	this.PostAutoSuggest();
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_sender");
			if (elm && !ew_HasValue(elm))
				return this.OnError(elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($transfers->sender->FldCaption()) ?>");
			elm = this.GetElements("x" + infix + "_account_no");
			if (elm && !ew_HasValue(elm))
				return this.OnError(elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($transfers->account_no->FldCaption()) ?>");
			elm = this.GetElements("x" + infix + "_transaction_amount");
			if (elm && !ew_HasValue(elm))
				return this.OnError(elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($transfers->transaction_amount->FldCaption()) ?>");
			elm = this.GetElements("x" + infix + "_transaction_amount");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($transfers->transaction_amount->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_transaction_id");
			if (elm && !ew_HasValue(elm))
				return this.OnError(elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($transfers->transaction_id->FldCaption()) ?>");
			elm = this.GetElements("x" + infix + "_transaction_date");
			if (elm && !ew_HasValue(elm))
				return this.OnError(elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($transfers->transaction_date->FldCaption()) ?>");
			elm = this.GetElements("x" + infix + "_transaction_date");
			if (elm && !ew_CheckUSDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($transfers->transaction_date->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_transaction_status");
			if (elm && !ew_HasValue(elm))
				return this.OnError(elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($transfers->transaction_status->FldCaption()) ?>");
			elm = this.GetElements("x" + infix + "_trnx_desc");
			if (elm && !ew_HasValue(elm))
				return this.OnError(elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($transfers->trnx_desc->FldCaption()) ?>");

			// Set up row object
			ew_ElementsToRow(fobj);

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
ftransfersadd.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
ftransfersadd.ValidateRequired = true;
<?php } else { ?>
ftransfersadd.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
ftransfersadd.Lists["x_account_no"] = {"LinkField":"x_ac_no","Ajax":true,"AutoFill":false,"DisplayFields":["x__email","x_user","x_ac_no",""],"ParentFields":[],"FilterFields":[],"Options":[]};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php $Breadcrumb->Render(); ?>
<?php $transfers_add->ShowPageHeader(); ?>
<?php
$transfers_add->ShowMessage();
?>
<form name="ftransfersadd" id="ftransfersadd" class="ewForm form-horizontal" action="<?php echo ew_CurrentPage() ?>" method="post">
<input type="hidden" name="t" value="transfers">
<input type="hidden" name="a_add" id="a_add" value="A">
<table cellspacing="0" class="ewGrid"><tr><td>
<table id="tbl_transfersadd" class="table table-bordered table-striped">
<?php if ($transfers->sender->Visible) { // sender ?>
	<tr id="r_sender"<?php echo $transfers->RowAttributes() ?>>
		<td><span id="elh_transfers_sender"><?php echo $transfers->sender->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></span></td>
		<td<?php echo $transfers->sender->CellAttributes() ?>><span id="el_transfers_sender" class="control-group">
<input type="text" data-field="x_sender" name="x_sender" id="x_sender" size="30" maxlength="25" placeholder="<?php echo $transfers->sender->PlaceHolder ?>" value="<?php echo $transfers->sender->EditValue ?>"<?php echo $transfers->sender->EditAttributes() ?>>
</span><?php echo $transfers->sender->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($transfers->account_no->Visible) { // account_no ?>
	<tr id="r_account_no"<?php echo $transfers->RowAttributes() ?>>
		<td><span id="elh_transfers_account_no"><?php echo $transfers->account_no->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></span></td>
		<td<?php echo $transfers->account_no->CellAttributes() ?>><span id="el_transfers_account_no" class="control-group">
<select data-field="x_account_no" id="x_account_no" name="x_account_no"<?php echo $transfers->account_no->EditAttributes() ?>>
<?php
if (is_array($transfers->account_no->EditValue)) {
	$arwrk = $transfers->account_no->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($transfers->account_no->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " selected=\"selected\"" : "";
		if ($selwrk <> "") $emptywrk = FALSE;
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $arwrk[$rowcntwrk][1] ?>
<?php if ($arwrk[$rowcntwrk][2] <> "") { ?>
<?php echo ew_ValueSeparator(1,$transfers->account_no) ?><?php echo $arwrk[$rowcntwrk][2] ?>
<?php } ?>
<?php if ($arwrk[$rowcntwrk][3] <> "") { ?>
<?php echo ew_ValueSeparator(2,$transfers->account_no) ?><?php echo $arwrk[$rowcntwrk][3] ?>
<?php } ?>
</option>
<?php
	}
}
?>
</select>
<?php
$sSqlWrk = "SELECT `ac_no`, `email` AS `DispFld`, `user` AS `Disp2Fld`, `ac_no` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `profile`";
$sWhereWrk = "";

// Call Lookup selecting
$transfers->Lookup_Selecting($transfers->account_no, $sWhereWrk);
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
?>
<input type="hidden" name="s_x_account_no" id="s_x_account_no" value="s=<?php echo ew_Encrypt($sSqlWrk) ?>&f0=<?php echo ew_Encrypt("`ac_no` = {filter_value}"); ?>&t0=3">
</span><?php echo $transfers->account_no->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($transfers->transaction_amount->Visible) { // transaction_amount ?>
	<tr id="r_transaction_amount"<?php echo $transfers->RowAttributes() ?>>
		<td><span id="elh_transfers_transaction_amount"><?php echo $transfers->transaction_amount->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></span></td>
		<td<?php echo $transfers->transaction_amount->CellAttributes() ?>><span id="el_transfers_transaction_amount" class="control-group">
<input type="text" data-field="x_transaction_amount" name="x_transaction_amount" id="x_transaction_amount" size="30" placeholder="<?php echo $transfers->transaction_amount->PlaceHolder ?>" value="<?php echo $transfers->transaction_amount->EditValue ?>"<?php echo $transfers->transaction_amount->EditAttributes() ?>>
</span><?php echo $transfers->transaction_amount->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($transfers->transaction_id->Visible) { // transaction_id ?>
	<tr id="r_transaction_id"<?php echo $transfers->RowAttributes() ?>>
		<td><span id="elh_transfers_transaction_id"><?php echo $transfers->transaction_id->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></span></td>
		<td<?php echo $transfers->transaction_id->CellAttributes() ?>><span id="el_transfers_transaction_id" class="control-group">
<input type="text" data-field="x_transaction_id" name="x_transaction_id" id="x_transaction_id" size="30" maxlength="15" placeholder="<?php echo $transfers->transaction_id->PlaceHolder ?>" value="<?php echo $transfers->transaction_id->EditValue ?>"<?php echo $transfers->transaction_id->EditAttributes() ?>>
</span><?php echo $transfers->transaction_id->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($transfers->transaction_date->Visible) { // transaction_date ?>
	<tr id="r_transaction_date"<?php echo $transfers->RowAttributes() ?>>
		<td><span id="elh_transfers_transaction_date"><?php echo $transfers->transaction_date->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></span></td>
		<td<?php echo $transfers->transaction_date->CellAttributes() ?>><span id="el_transfers_transaction_date" class="control-group">
<input type="text" data-field="x_transaction_date" name="x_transaction_date" id="x_transaction_date" placeholder="<?php echo $transfers->transaction_date->PlaceHolder ?>" value="<?php echo $transfers->transaction_date->EditValue ?>"<?php echo $transfers->transaction_date->EditAttributes() ?>>
<?php if (!$transfers->transaction_date->ReadOnly && !$transfers->transaction_date->Disabled && @$transfers->transaction_date->EditAttrs["readonly"] == "" && @$transfers->transaction_date->EditAttrs["disabled"] == "") { ?>
<button id="cal_x_transaction_date" name="cal_x_transaction_date" class="btn" type="button"><img src="phpimages/calendar.png" id="cal_x_transaction_date" alt="<?php echo $Language->Phrase("PickDate") ?>" title="<?php echo $Language->Phrase("PickDate") ?>" style="border: 0;"></button><script type="text/javascript">
ew_CreateCalendar("ftransfersadd", "x_transaction_date", "%m/%d/%Y");
</script>
<?php } ?>
</span><?php echo $transfers->transaction_date->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($transfers->transaction_status->Visible) { // transaction_status ?>
	<tr id="r_transaction_status"<?php echo $transfers->RowAttributes() ?>>
		<td><span id="elh_transfers_transaction_status"><?php echo $transfers->transaction_status->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></span></td>
		<td<?php echo $transfers->transaction_status->CellAttributes() ?>><span id="el_transfers_transaction_status" class="control-group">
<input type="text" data-field="x_transaction_status" name="x_transaction_status" id="x_transaction_status" size="30" maxlength="15" placeholder="<?php echo $transfers->transaction_status->PlaceHolder ?>" value="<?php echo $transfers->transaction_status->EditValue ?>"<?php echo $transfers->transaction_status->EditAttributes() ?>>
</span><?php echo $transfers->transaction_status->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($transfers->trnx_desc->Visible) { // trnx_desc ?>
	<tr id="r_trnx_desc"<?php echo $transfers->RowAttributes() ?>>
		<td><span id="elh_transfers_trnx_desc"><?php echo $transfers->trnx_desc->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></span></td>
		<td<?php echo $transfers->trnx_desc->CellAttributes() ?>><span id="el_transfers_trnx_desc" class="control-group">
<textarea data-field="x_trnx_desc" class="editor" name="x_trnx_desc" id="x_trnx_desc" cols="35" rows="4" placeholder="<?php echo $transfers->trnx_desc->PlaceHolder ?>"<?php echo $transfers->trnx_desc->EditAttributes() ?>><?php echo $transfers->trnx_desc->EditValue ?></textarea>
<script type="text/javascript">
ew_CreateEditor("ftransfersadd", "x_trnx_desc", 35, 4, <?php echo ($transfers->trnx_desc->ReadOnly || FALSE) ? "true" : "false" ?>);
</script>
</span><?php echo $transfers->trnx_desc->CustomMsg ?></td>
	</tr>
<?php } ?>
</table>
</td></tr></table>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("AddBtn") ?></button>
</form>
<script type="text/javascript">
ftransfersadd.Init();
<?php if (EW_MOBILE_REFLOW && ew_IsMobile()) { ?>
ew_Reflow();
<?php } ?>
</script>
<?php
$transfers_add->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$transfers_add->Page_Terminate();
?>
