<div class="row">
    		  		<div class="col-md-12">
    		  			<div class="content-box-large">
    		  				<div class="panel-heading">
      							<div class="panel-title"><h3>Recent Transactions</h3></div>
      
      							<div class="panel-options">
      								<a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
      								<a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
      							</div>
      						</div>
		  				    <div class="panel-body">
                            <?php if ($result){ 

                              ?>
                                    <table class="table  table-bordered">
                                      <tbody>
                                        <tr align="center" valign="middle" style="color:White;background-color:Gray;font-weight:bold;font-style:normal;text-decoration:none;">
                                              <td align="center" valign="middle" style="font-weight:bold;font-style:normal;text-decoration:none;">
                                                Account Name
                                              </td>
                                              <td>Reference</td>
                                              <td align="center" style="font-weight:bold;font-style:normal;text-decoration:none;">
                                                Amount
                                              </td>
                                              <td>Date Completed</td>
                                            </tr>
                                            <?php 
                                              if (!isset($raar) || count($raar) === 0) {
                                                echo "<h5>No recent transactions.</h5>";
                                              } else {                                              
                                                foreach ($raar as $key => $trRow) {
                                                  $explodeDesc = json_decode($trRow['trnx_desc']);
                                                  // var_dump($explodeDesc);
                                                  $tfSender = isset($explodeDesc->{'ben-b-name'}) ? $explodeDesc->{'ben-b-name'} : @$explodeDesc->{'s-b-name'} ;
                                                  $tfAccount = $trRow['account_no'];
                                                  $transaction_id = $trRow['transaction_id'];
                                                  $transaction_status = $trRow['transaction_status'];
                                                  $transaction_amount = $trRow['transaction_amount'];                                              
                                                  $transaction_date = $trRow['transaction_date']; 
                                                  $transferMethod = $explodeDesc->{'transferMethod'}; 
                                            ?>
                                                  <tr align="center" style="font-weight:normal;font-style:normal;text-decoration:none;">
                              
                                                    <td align="center" style="font-weight:normal;font-style:normal;text-decoration:none;">
                                                      <p style="font-size: 13px"><?php echo @$tfSender; ?></em></p>
                                                    </td>
                                                    <td align="center" style="font-weight:normal;font-style:normal;text-decoration:none;">
                                                      <p style="font-size: 13px"><?php echo @$transaction_id; ?>&nbsp; <em><?php echo $transferMethod; ?></p>
                                                    </td>
                                                    <td align="center" style="font-weight:normal;font-style:normal;text-decoration:none;">
                                                      <p style="font-size: 13px"><?php echo "USD$ " . @number_format($transaction_amount); ?></p>
                                                    </td>
                                                    <td align="center" style="font-weight:normal;font-style:normal;text-decoration:none;">
                                                      <p style="font-size: 13px"><?php echo @$transaction_date ? '<span class="small-text">Done&nbsp;-&nbsp;</span>'.$transaction_date: 'Pending' ; ?></p>
                                                    </td>
                                                  </tr>
                                            <?php
                                                }
                                              }
                                            ?>
                                        </tbody>
                                    </table>
                            <?php } ?>
    		  				</div>
    		  			</div>
    		  		</div>
    		  	</div>