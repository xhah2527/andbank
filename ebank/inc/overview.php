<div class="row">
              <div class="col-md-12">
                <div class="content-box-large">
                  <div class="panel-heading">
                    <div class="panel-title"><h3>Account Overview</h3></div>
                  </div>
                  <div class="panel-body">
                    <table class="table table-striped">
                        <tbody>
                            <tr align="center" valign="middle" style="font-weight:bold;font-style:normal;text-decoration:none;">
                            <td align="center" valign="middle" style="font-weight:bold;font-style:normal;text-decoration:none;">Account Number</td><td>Internal Reference</td><td align="center" style="font-weight:bold;font-style:normal;text-decoration:none;">Account Type</td><td align="center">Currency </td><td> Balance</td><td>Account Status</td>
                          </tr><tr align="center" style="font-weight:normal;font-style:normal;text-decoration:none;">
                            <td align="center" style="font-weight:normal;font-style:normal;text-decoration:none;"><a href="#"><?php echo $ac_no; ?></a></td><td align="center" style="font-weight:normal;font-style:normal;text-decoration:none;">758/382048/1/21/0</td><td align="center" style="font-weight:normal;font-style:normal;text-decoration:none;">CURRENT ACCOUNT</td><td align="center"><?php echo $currency; ?> </td><td align="right">         <?php echo number_format($a_balance); ?></td><td align="center" style="font-weight:normal;font-style:normal;text-decoration:none;">Active</td>
                          </tr>
                        </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>