          <div class="col-md-3 r-sidebar">
            <div class="row">
              <div class="col-md-12">
                <div class="content-box-large box-with-header">
                 
                  <h3>
                    <small>Welcome</small> <br /><b><?php echo "$firstname $lastname."; ?></b>
                  </h3>
                  <p>
                    <?php
                      if (isset($_SESSION['user']['image']) && strlen($_SESSION['user']['image']) > 3 ) {
                        $image = $_SESSION['user']['image'];
                      } else {
                        $image =  HOST.'/images/proxy.duckduckgo.com.png';
                      }
                    ?>
                    <img src="<?php echo $image; ?>"  style="width:240px">
                  </p>
                  <hr>
                  <p>Current Balance:</p>
                  <p style="
                      font-weight: 800;
                      font-size: 25px;
                      position: relative;
                      top: -7px;
                  ">
                    <small style="
                        font-size: smaller;
                        font-weight: 100;
                    "><?php echo $currency; ?>
                    </small>
                    <?php echo $a_balance; ?>
                  </p>
                  <hr>
                </div>
              </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <p>
                        Ensure no one is looking over your shoulders as you use your card. Make sure you observe your surroundings.
                    </p>
                    <p>
                        Your Account User ID and Password are confidential. Do not disclose them to anyone.
                    </p>
                </div>
            </div>
          </div>