<div class="sidebar content-box" style="display: block;">
    <div class="dropdown mobile-nav">
        <a class="btn btn-secondary dropdown-toggle btn-block" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Menu
        </a>
      <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
        <a class="dropdown-item" href="<?php echo HOST."/account-management.php"; ?>"><i class="fas fa-file-invoice-dollar"></i>&nbsp; My Account</a>
        <a class="dropdown-item" href='<?php echo HOST."/money-transfer.php"; ?>'><i class="fas fa-hand-holding-usd"></i>&nbsp; Wire Transfer</a>
        <a class="dropdown-item" href='<?php echo HOST."/own-transfer.php"; ?>'><i class="fas fa-piggy-bank"></i>&nbsp; Own Transfer</a>
        <a class="dropdown-item" href='<?php echo HOST."/andbank-transfer.php"; ?>'><i class="fas fa-money-bill"></i>&nbsp; AndBank Transfer</a>
        <a class="dropdown-item" href="<?php echo HOST."/bill-payment.php"; ?>" flag-on><i class="fas fa-money-check-alt"></i>&nbsp; Bill Payment</a>
        <a class="dropdown-item" href="<?php echo HOST."/cards.php"; ?>" flag-on><i class="fas fa-credit-card"></i>&nbsp; Cards</a>
        <a class="dropdown-item" href="<?php echo HOST."/account-services.php"; ?>" flag-on><i class="far fa-money-bill-alt"></i>&nbsp; Account Services</a>
        <a class="dropdown-item" href="<?php echo HOST. "/logout.php"; ?>"><i class="fas fa-sign-out-alt"></i>&nbsp; Logout</a>    

      </div>

    </div>
    <ul class="nav">
        <!-- Main menu -->
        <li><a href="<?php echo HOST."/account-management.php"; ?>"><i class="fas fa-file-invoice-dollar"></i>&nbsp; My Account</a></li>
        <li><a href='<?php echo HOST."/money-transfer.php"; ?>'><i class="fas fa-hand-holding-usd"></i>&nbsp; Wire Transfer</a></li>
        <li><a href='<?php echo HOST."/own-transfer.php"; ?>'><i class="fas fa-piggy-bank"></i>&nbsp; Own Transfer</a></li>
        <li><a href='<?php echo HOST."/andbank-transfer.php"; ?>'><i class="fas fa-money-bill"></i>&nbsp; AndBank Transfer</a></li>
        <li><a href="<?php echo HOST."/bill-payment.php"; ?>" flag-on><i class="fas fa-money-check-alt"></i>&nbsp; Bill Payment</a></li>
        <li><a href="<?php echo HOST."/cards.php"; ?>" flag-on><i class="fas fa-credit-card"></i>&nbsp; Cards</a></li>
        <li><a href="<?php echo HOST."/account-services.php"; ?>" flag-on><i class="far fa-money-bill-alt"></i>&nbsp; Account Services</a></li>
        <li><a href="<?php echo HOST. "/logout.php"; ?>"><i class="fas fa-sign-out-alt"></i>&nbsp; Logout</a></li>
    </ul>
 </div>