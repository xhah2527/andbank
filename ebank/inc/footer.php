    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.2/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
<footer>
         <div class="container">

            <div class="copy text-center">
               Copyright 2019 <a href='#'>
                 <?php echo COPYRIGHT; ?>
               </a>
            </div>

         </div>
    </footer>