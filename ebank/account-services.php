<?php
  include './boot.php';
  $fromAccount = $currentUser[9];
  $query = "SELECT * FROM transfers WHERE account_no = '$fromAccount' ORDER BY transaction_date DESC, id DESC";

  if(!$result = $db->query($query)){
      die('There was an error running the query [' . $db->error . ']');
  }

  if(mysqli_num_rows($result) > 0){
    $raar = mysqli_fetch_array($result);
    // $raar = mysql_fetch_array($qryagain, MYSQL_ASSOC);
    $tfAccount = $raar['account_no'];
    $transaction_id = $raar['transaction_id'];
    $transaction_status = $raar['transaction_status'];
    $transaction_amount = $raar['transaction_amount'];
  }


  // $qryagain = mysql_query() or die(mysql_error());
  // if(mysql_num_rows($qryagain)>0){
  //   $raar = mysql_fetch_array($qryagain, MYSQL_ASSOC);
  //   $tfAccount = $raar['account_no'];
  //   $transaction_id = $raar['transaction_id'];
  //   $transaction_status = $raar['transaction_status'];
  //   $transaction_amount = $raar['transaction_amount'];
  // }
?>
<!DOCTYPE html>
<html>
    <head>

    <?php include_once './inc/head.php'; ?>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/animatecss/3.5.2/animate.min.css">
    <link rel="stylesheet" type="text/css" href="css/morphext.css">
    <script type="text/javascript" src="https://cdn.jsdelivr.net/lodash/4.13.1/lodash.min.js"></script>
  </head>
  <body>
  	<?php include_once './inc/header.php'; ?>
    <div class="page-content">
    	<div class="row">
  		    <div class="col-md-2">
		  	    <?php include_once './inc/nav.php'; ?>
    		</div>
		    <div class="col-md-7">
          <h3>Account Profile</h3>
                            <div class="content-box-large">
                    <div class="panel-heading">
                      <div class="panel-title text-center"><strong>Unavailable</strong></div>
                    </div>
                    <div class="panel-body">
                      <p>
                        You need an elevated priviledge to make changes to your account.<br>
                        Contact your AndBank account officer .
                      </p>
                    </div>
                  </div>
		  	</div>
            <?php include_once './inc/sidebar.php'; ?>
      </div>
  	</div>
    <?php include_once './inc/footer.php'; ?>
    <?php include 'modal.php'; ?>


<script type="text/javascript">


  $(document).on('click', '.send-request-tf', function (e){
    e.preventDefault();
    $('.modal').modal('show');
    var em  = $('#emailinput').val();
    var mp = $('#mobilenoinput').val();
    var bname = $('.ben-b-name').val();
    var cust = $('.ben-cust-name').val();
    var tranamount = $('.transaction-amount').val();
    var trandesc = $('.transaction-desc').val();
    var code = _.random(1111, 9999);
    var mobileNo = $('#mobilenoinput').val();
    var userInputElements = $('form.quickly-form .form-control');

    setTimeout(function () {

      $.ajax({
        url: 'process-transfer.php',
        method: 'POST',
        async: false,
        data:userInputElements.serialize(),
        success: function (data) {
          alert('This transaction has been submitted. You will get a confirmation message soon. Thank you');
          console.log(data);
          $('.modal').modal('hide');
          var windowObjectReference;

          var strWindowFeatures = "menubar=no,location=no,resizable=yes,scrollbars=yes,status=no";

          function openRequestedPopup(url) {
            windowObjectReference = window.open(url, "Transaction Receipt", strWindowFeatures);
          }


          var recipt = '<?php echo $host; ?>/transaction-receipt.php?ben-cust-name=' + cust+ '&transaction-amount=' + tranamount;

          openRequestedPopup(recipt);

          $('.send-request-tf').text('Transfer Done').prop('disabled', 'disabled');
        }
      });
    }, 1000)
  });
</script>

  </body>
</html>