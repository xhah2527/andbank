<?php 
    include './boot.php';
  ?>
<!DOCTYPE html>
<html>
    <head>

    <?php include_once './inc/head.php'; ?>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/animatecss/3.5.2/animate.min.css">
    <link rel="stylesheet" type="text/css" href="css/morphext.css">
    <script type="text/javascript" src="https://cdn.jsdelivr.net/lodash/4.13.1/lodash.min.js"></script>
  </head>
  <body>
  	<?php include_once './inc/header.php'; ?>
    <div class="page-content">
    	<div class="row">
		    <div class="col-md-2">
		  	    <?php include_once './inc/nav.php'; ?>
    		</div>
        <div class="col-md-7">
          <div class="row">        
    		    <?php
              if ($_POST['transferMethod'] === 'incoming-funds') {
                include './inc/incoming-tf.php';
              }
              if ($_POST['transferMethod'] === 'outgoing-funds') {
                include './inc/outgoing-tf.php';
              }
            ?>
          </div>
        </div>  
        <?php include_once './inc/sidebar.php'; ?>
      </div>
  	</div>
    <?php include_once './inc/footer.php'; ?>

  </body>
</html>